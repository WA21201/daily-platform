create table da_auth
(
    AUTHID   varchar(3)  not null comment '权限id',
    AUTHNAME varchar(10) not null comment '权限名称',
    AUTHDESC varchar(30) null comment '权限描述',
    constraint da_auth_AUTHID_uindex
        unique (AUTHID)
)
    comment '权限表';

alter table da_auth
    add primary key (AUTHID);

create table da_log_level
(
    LEVELID   varchar(10) not null comment '日志级别id',
    LEVELNAME varchar(10) not null comment '日志级别名',
    STATUS    varchar(1)  not null comment '是否使用，0：否，1：是，默认1',
    constraint da_log_level_LEVELID_uindex
        unique (LEVELID)
)
    comment '日志级别表';

alter table da_log_level
    add primary key (LEVELID);

create table da_menu
(
    MENUID     varchar(10) not null comment '菜单id',
    MENUCODE   varchar(3)  not null comment '菜单码',
    FATHERID   varchar(10) not null comment '父级菜单id,0表示为父级',
    MENUNAME   varchar(50) null comment '菜单名',
    CREATETIME varchar(20) not null comment '添加日期',
    STATUS     varchar(1)  not null comment '是否激活 0：否，1：是，默认1',
    constraint da_menu_MENUCODE_uindex
        unique (MENUCODE),
    constraint da_menu_MENUID_uindex
        unique (MENUID)
)
    comment '菜单表';

alter table da_menu
    add primary key (MENUID);

create table da_ope_log
(
    LOGID      varchar(50)  not null comment '日志id',
    USERID     varchar(32)  not null comment '操作员id',
    CLAZZ      varchar(100) not null comment '操作类',
    METHOD     varchar(100) null comment '操作方法',
    LEVELID    varchar(10)  not null comment '日志级别id',
    REMARK     varchar(100) not null comment '操作备注',
    CREATETIME varchar(20)  not null comment '操作日期',
    constraint da_ope_log_LOGID_uindex
        unique (LOGID)
)
    comment '操作日志表';

alter table da_ope_log
    add primary key (LOGID);

create table da_problem
(
    PROBLEMID varchar(3)   not null comment '问题id',
    PROBLEM   varchar(100) null comment '问题',
    constraint da_problem_PROBLEMID_uindex
        unique (PROBLEMID)
)
    comment '问题表';

alter table da_problem
    add primary key (PROBLEMID);

create table da_user
(
    USERID     varchar(32)  not null comment '用户id',
    USERNAME   varchar(20)  not null comment '账号',
    PASSWORD   varchar(50)  not null comment '密码',
    NICKNAME   varchar(20)  null comment '昵称',
    AUTHID     varchar(3)   not null comment '权限id',
    HEADIMAGE  varchar(100) null comment '头像',
    CREATETIME varchar(20)  not null comment '创建时间',
    STATUS     varchar(1)   not null comment '是否激活，0：否，1：是，默认1',
    PROBLEM1   varchar(3)   null comment '问题1',
    ANSWER1    varchar(100) null comment '答案1',
    constraint DA_USER_USERID_uindex
        unique (USERID),
    constraint DA_USER_USERNAME_uindex
        unique (USERNAME)
)
    comment '用户表';

alter table da_user
    add primary key (USERID);

create table da_user_menu
(
    USERMENUID varchar(10) not null comment '用户菜单id',
    USERID     varchar(32) not null comment '用户id',
    MENUID     varchar(10) not null comment '菜单id',
    STATUS     varchar(1)  null comment '是否激活，0：否，1：是，默认1',
    constraint da_user_menu_USERMENUID_uindex
        unique (USERMENUID)
)
    comment '用户菜单表';

alter table da_user_menu
    add primary key (USERMENUID);


