package com.wang.application;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(scanBasePackages = {"com.wang.common", "com.wang.transfer", "com.wang.application"})
@MapperScan({"com.wang.application.mapper", "com.wang.common.mapper"})
@EnableCaching
public class DailyPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(DailyPlatformApplication.class, args);
    }

}
