package com.wang.application.api.menu.deleteMenu;

import lombok.Data;

import java.io.Serializable;

/**
 * 删除菜单请求体
 */
@Data
public class DeleteMenuRequest implements Serializable {

    private static final long serialVersionUID = 1398634687973479811L;

    /**
     * 菜单ID
     */
    private String menuid;
}
