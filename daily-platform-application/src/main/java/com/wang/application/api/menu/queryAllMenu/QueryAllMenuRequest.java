package com.wang.application.api.menu.queryAllMenu;

import com.wang.application.model.DaMenu;
import com.wang.common.common.QRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 查询所有菜单请求体
 * @since 2022.11.11
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryAllMenuRequest extends QRequest<DaMenu> implements Serializable {
    private static final long serialVersionUID = -917052610472940711L;

    private String menuid;

    private String menucode;

    private String fatherid;

    private String menuname;

    private String status;

    @Override
    public DaMenu equalTransfer() {
        DaMenu daMenu = new DaMenu();
        daMenu.setStatus(status);
        daMenu.setMenuid(menuid);
        daMenu.setFatherid(fatherid);
        return daMenu;
    }

    @Override
    public DaMenu likeTransfer() {
        DaMenu daMenu = new DaMenu();
        daMenu.setMenuname(menuname);
        daMenu.setMenucode(menucode);
        return daMenu;
    }
}
