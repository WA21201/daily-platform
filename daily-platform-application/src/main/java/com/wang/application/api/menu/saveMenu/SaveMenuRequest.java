package com.wang.application.api.menu.saveMenu;

import com.wang.application.model.DaMenu;
import com.wang.common.common.ARequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 新增/保存菜单请求体
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SaveMenuRequest extends ARequest<DaMenu> implements Serializable {

    private static final long serialVersionUID = 4641900061243541171L;

    /**
     * 菜单id
     */
    private String menuid;

    /**
     * 菜单码
     */
    private String menucode;

    /**
     * 父级菜单id,0表示为父级
     */
    private String fatherid;

    /**
     * 菜单名
     */
    private String menuname;

    /**
     * 添加日期
     */
    private String createtime;

    /**
     * 是否激活 0：否，1：是，默认1
     */
    private String status;

    /**
     * 操作类型 0：新增 1：修改
     */
    private String operation;

    @Override
    public DaMenu transfer() {
        DaMenu daMenu = new DaMenu();
        daMenu.setMenuid(menuid);
        daMenu.setMenucode(menucode);
        daMenu.setFatherid(fatherid);
        daMenu.setMenuname(menuname);
        daMenu.setCreatetime(createtime);
        daMenu.setStatus(status);
        return daMenu;
    }
}
