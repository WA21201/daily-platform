package com.wang.application.api.user;

import com.wang.application.model.DaUser;
import com.wang.common.common.ARequest;
import com.wang.common.utils.Utils;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户登录请求体
 */
@Data
public class LoginRequest extends ARequest<DaUser> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    @Override
    public DaUser transfer() {
        DaUser daUser = new DaUser();
        daUser.setUsername(this.getUsername());
        daUser.setPassword(Utils.encodePassword(this.getPassword()));
        return daUser;
    }
}
