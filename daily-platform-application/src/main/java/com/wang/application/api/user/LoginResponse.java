package com.wang.application.api.user;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 用户登录返回体
 */
@Data
@XmlRootElement(name = "LoginResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class LoginResponse {

    /**
     * 用户id
     */
    private String userid;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 权限id
     */
    private String authid;

    /**
     * 头像
     */
    private String headimage;

    /**
     * 创建时间
     */
    private String createtime;

    /**
     * 是否激活，0：否，1：是，默认1
     */
    private String status;

    /**
     * 问题1
     */
    private String problem1;

    /**
     * 答案1
     */
    private String answer1;

    /**
     * token
     */
    private String token;
}
