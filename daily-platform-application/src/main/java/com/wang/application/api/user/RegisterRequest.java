package com.wang.application.api.user;

import com.wang.application.model.DaUser;
import com.wang.common.common.ARequest;
import com.wang.common.utils.Contact;
import com.wang.common.utils.Utils;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求体
 */
@Data
public class RegisterRequest extends ARequest<DaUser> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    private String checkPassword;

    private String nickname;

    private String headimage;

    private String problem1;

    private String answer1;

    @Override
    public DaUser transfer() {
        DaUser daUser = new DaUser();
        daUser.setUserid(Utils.genUserid());
        daUser.setUsername(this.getUsername());
        daUser.setPassword(Utils.encodePassword(this.getPassword()));
        daUser.setNickname(Utils.isEmpty(this.getNickname()) ? this.getUsername() : this.getNickname());
        daUser.setAuthid(Contact.Auth.MINAUTH); // 最低权限
        daUser.setHeadimage(this.getHeadimage());
        daUser.setCreatetime(Utils.convertDateToString());
        daUser.setStatus(Contact.Status.ACTIVE);// 默认激活
        daUser.setProblem1(this.getProblem1());
        daUser.setAnswer1(this.getAnswer1());
        return daUser;
    }
}
