package com.wang.application.api.user;

import com.wang.application.model.DaUser;
import com.wang.common.common.ARequest;
import com.wang.common.utils.Utils;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户修改个人信息请求体
 */
@Data
public class UpdateRequest extends  ARequest<DaUser> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userid;

    private String username;

    private String password;

    private String checkPassword;

    private String nickname;

    private String authid;

    private String headimage;

    private String status;

    private String problem1;

    private String answer1;

    @Override
    public DaUser transfer() {
        DaUser daUser = new DaUser();
        daUser.setUserid(this.getUserid());
        daUser.setUsername(this.getUsername());
        daUser.setPassword(Utils.encodePassword(this.getPassword()));
        daUser.setNickname(this.getNickname());
        daUser.setAuthid(this.getAuthid());
        daUser.setHeadimage(this.getHeadimage());
        daUser.setStatus(this.getStatus());
        daUser.setProblem1(this.getProblem1());
        daUser.setAnswer1(this.getAnswer1());
        return daUser;
    }
}
