package com.wang.application.api.userMenu.queryUserMenu;

import lombok.Data;

import java.util.List;

/**
 * 所有用户列表
 */
@Data
public class AllUserList {

    private String userid;

    private String username;

    private String nickname;

    private String authid;

    private String createtime;

    private List<MenuFatherList> menuFatherLists;
}
