package com.wang.application.api.userMenu.queryUserMenu;

import lombok.Data;

@Data
public class LeftMenuList {

    private String menuid;

    private String menucode;

    private String fatherid;

    private String menuname;

    private String createtime;

    private String status;
}
