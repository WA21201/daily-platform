package com.wang.application.api.userMenu.queryUserMenu;

import com.wang.application.model.DaUser;
import com.wang.common.common.QRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 查询所有用户菜单分页请求体
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryAllUserMenuRequest extends QRequest<DaUser> implements Serializable {

    private static final long serialVersionUID = 3488593359912879928L;

    private String userid;

    private String username;

    private String nickname;

    @Override
    public DaUser equalTransfer() {
        return new DaUser();
    }

    @Override
    public DaUser likeTransfer() {
        DaUser daUser = new DaUser();
        daUser.setUserid(userid);
        daUser.setUsername(username);
        daUser.setNickname(nickname);
        return daUser;
    }
}
