package com.wang.application.api.userMenu.queryUserMenu;

import com.wang.application.model.DaUserMenu;
import com.wang.common.common.ARequest;
import lombok.Data;

/**
 * 查询用户菜单请求体
 */
@Data
public class QueryUserMenuRequest extends ARequest<DaUserMenu> {
    
    
    
    @Override
    public DaUserMenu transfer() {
        return null;
    }
}
