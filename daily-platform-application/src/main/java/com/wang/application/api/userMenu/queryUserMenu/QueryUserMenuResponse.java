package com.wang.application.api.userMenu.queryUserMenu;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * 查询用户菜单返回体
 */
@Data
@XmlRootElement(name = "queryUserMenuResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryUserMenuResponse implements Serializable {

    private static final long serialVersionUID = -7541720657711373729L;

    private List<MenuFatherList> menuFatherLists;
}
