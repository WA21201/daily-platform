package com.wang.application.api.userMenu.queryUserMenu;

import lombok.Data;

import java.util.List;

@Data
public class UserMenuList {

    private String menuid;

    private String menucode;

    private String fatherid;

    private String menuname;

    private String createtime;

    private String status;

    private List<LeftMenuList> leftMenuLists;
}
