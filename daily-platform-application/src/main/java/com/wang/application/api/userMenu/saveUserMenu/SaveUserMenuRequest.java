package com.wang.application.api.userMenu.saveUserMenu;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 新增/保存用户菜单请求体
 */
@Data
public class SaveUserMenuRequest implements Serializable {

    private static final long serialVersionUID = -2697338803246867041L;

    private String userid;

    private List<String> menuidlists;
}
