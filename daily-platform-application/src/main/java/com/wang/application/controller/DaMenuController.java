package com.wang.application.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wang.application.api.menu.deleteMenu.DeleteMenuRequest;
import com.wang.application.api.menu.queryAllMenu.QueryAllMenuRequest;
import com.wang.application.api.menu.saveMenu.SaveMenuRequest;
import com.wang.application.model.DaMenu;
import com.wang.application.service.DaMenuService;
import com.wang.common.annotation.Log;
import com.wang.common.common.AResponse;
import com.wang.common.common.AbstractController;
import com.wang.common.utils.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 菜单接口
 */
@RestController
@RequestMapping("/menu")
@Slf4j
public class DaMenuController extends AbstractController {

    @Resource
    private DaMenuService daMenuService;

    /**
     * 分页查询所有菜单
     * @param request 查询条件
     * @return AResponse<Page<DaMenu>>
     */
    @PostMapping("/queryMenu")
    @Log(clazz = "DaMenuController", method = "queryAllMenu")
    public AResponse<Page<DaMenu>> queryAllMenu(@RequestBody QueryAllMenuRequest request) {
        Page<DaMenu> page = daMenuService.queryAllMenu(request);
        return ResultUtils.success(page);
    }

    /**
     * 保存/新增菜单
     * @param request 菜单信息
     * @return AResponse<Boolean>
     */
    @PostMapping("/saveMenu")
    @Log(clazz = "DaMenuController", method = "saveMenu")
    public AResponse<Boolean> saveMenu(@RequestBody SaveMenuRequest request) {
        Boolean result = daMenuService.saveMenu(request);
        return ResultUtils.success(result);
    }

    /**
     * 删除菜单
     * @param request 菜单ID
     * @return AResponse<Boolean>
     */
    @PostMapping("/deleteMenu")
    @Log(clazz = "DaMenuController", method = "deleteMenu")
    public AResponse<Boolean> deleteMenu(@RequestBody DeleteMenuRequest request) {
        Boolean result = daMenuService.deleteMenu(request);
        return ResultUtils.success(result);
    }
}
