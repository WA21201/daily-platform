package com.wang.application.controller;

import com.wang.application.model.DaProblem;
import com.wang.application.service.DaProblemService;
import com.wang.common.annotation.Log;
import com.wang.common.common.AResponse;
import com.wang.common.common.AbstractController;
import com.wang.common.common.ErrorCode;
import com.wang.common.exception.BusinessException;
import com.wang.common.utils.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 问题接口
 */
@RestController
@RequestMapping("/problem")
@Slf4j
public class DaProblemController extends AbstractController {

    @Resource
    private DaProblemService daProblemService;

    /**
     * 查询问题列表
     * @return AResponse<List<DaProblem>>
     */
    @GetMapping("/queryProblem")
    @Log(clazz = "DaProblemController", method = "queryProblems")
    public AResponse<List<DaProblem>> queryProblems() {
        List<DaProblem> daProblems = daProblemService.queryProblems();
        if (daProblems == null || daProblems.size() < 1) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "问题列表查询失败");
        }
        return ResultUtils.success(daProblems);
    }
}
