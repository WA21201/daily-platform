package com.wang.application.controller;

import com.wang.application.api.user.LoginRequest;
import com.wang.application.api.user.LoginResponse;
import com.wang.application.api.user.RegisterRequest;
import com.wang.application.api.user.UpdateRequest;
import com.wang.application.model.DaUser;
import com.wang.application.service.DaUserService;
import com.wang.common.annotation.Log;
import com.wang.common.common.AResponse;
import com.wang.common.common.AbstractController;
import com.wang.common.common.ErrorCode;
import com.wang.common.exception.BusinessException;
import com.wang.common.utils.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 用户接口
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class DaUserController extends AbstractController {

    @Resource
    private DaUserService daUserService;

    /**
     * 用户注册账号
     * @param request 注册信息
     * @return AResponse<Integer>
     */
    @PostMapping("/register")
    @Log(clazz = "DaUserController", method = "userRegister")
    public AResponse<Integer> userRegister(@RequestBody RegisterRequest request) {
        Boolean result = daUserService.register(request);
        if (!result) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "注册失败");
        }
        return ResultUtils.success(1);
    }

    /**
     * 用户登录
     * @param request 登录信息
     * @return AResponse<LoginResponse>
     */
    @PostMapping("/login")
    @Log(clazz = "DaUserController", method = "userLogin")
    public AResponse<LoginResponse> userLogin(@RequestBody LoginRequest request) {
        LoginResponse result = daUserService.login(request);
        if (result == null) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "登录失败");
        }
        return ResultUtils.success(result);
    }

    /**
     * 用户登出
     * @return AResponse<Integer>
     */
    @GetMapping("/logout")
    @Log(clazz = "DaUserController", method = "userLogout")
    public AResponse<Integer> userLogout() {
        Boolean logout = daUserService.logout();
        if (!logout) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "退出登录失败");
        }
        return ResultUtils.success(1);
    }

    /**
     * 用户头像上传
     * @param file 头像文件
     * @return AResponse<DaUser>
     */
    @PostMapping("/uploadHeadImage")
    @Log(clazz = "DaUserController", method = "uploadHeadImage")
    public AResponse<DaUser> uploadHeadImage(MultipartFile file) {
        DaUser daUser = daUserService.uploadHeadImage(file);
        return ResultUtils.success(daUser);
    }

    /**
     * 修改用户信息
     * @param request 用户信息
     * @return AResponse<DaUser>
     */
    @PostMapping("/updateUserInfo")
    @Log(clazz = "DaUserController", method = "updateUserInfo")
    public AResponse<DaUser> updateUserInfo(@RequestBody UpdateRequest request) {
        DaUser daUser = daUserService.updateUserInfo(request);
        return ResultUtils.success(daUser);
    }

    /**
     * 忘记密码
     * @param request 用户信息
     * @return AResponse<Boolean>
     */
    @PostMapping("/forgetPassword")
    @Log(clazz = "DaUserController", method = "forgetPassword")
    public AResponse<Boolean> forgetPassword(@RequestBody UpdateRequest request) {
        Boolean bol = daUserService.forgetPassword(request);
        if (!bol) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "密码修改失败");
        }
        return ResultUtils.success(true);
    }
}
