package com.wang.application.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wang.application.api.userMenu.queryUserMenu.AllUserList;
import com.wang.application.api.userMenu.queryUserMenu.QueryAllUserMenuRequest;
import com.wang.application.api.userMenu.queryUserMenu.QueryUserMenuResponse;
import com.wang.application.api.userMenu.saveUserMenu.SaveUserMenuRequest;
import com.wang.application.service.DaUserMenuService;
import com.wang.common.annotation.Log;
import com.wang.common.common.AResponse;
import com.wang.common.common.AbstractController;
import com.wang.common.common.ErrorCode;
import com.wang.common.exception.BusinessException;
import com.wang.common.utils.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Slf4j
@RequestMapping("/userMenu")
public class DaUserMenuController extends AbstractController {

    @Resource
    private DaUserMenuService daUserMenuService;

    /**
     * 查询用户菜单
     * @return AResponse<QueryUserMenuResponse>
     */
    @GetMapping("/queryUserMenu")
    @Log(clazz = "DaUserMenuController", method = "queryUserMenu")
    public AResponse<QueryUserMenuResponse> queryUserMenu() {
        QueryUserMenuResponse queryUserMenuResponse = daUserMenuService.queryUserMenu();
        if (queryUserMenuResponse.getMenuFatherLists() == null || queryUserMenuResponse.getMenuFatherLists().size() < 1)
            throw new BusinessException(ErrorCode.NO_MENU, "用户未配置菜单");
        return ResultUtils.success(queryUserMenuResponse);
    }

    /**
     * 所有用户菜单分页查询
     * @return AResponse<Page<AllUserList>>
     */
    @PostMapping("/queryAllUserMenu")
    @Log(clazz = "DaUserMenuController", method = "queryAllUserMenu")
    public AResponse<Page<AllUserList>> queryAllUserMenu(@RequestBody QueryAllUserMenuRequest request) {
        Page<AllUserList> page = daUserMenuService.queryAllUserMenu(request);
        if (page.getRecords() == null || page.getRecords().size() < 1) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "所有用户菜单分页查询失败");
        }
        return ResultUtils.success(page);
    }

    /**
     * 保存用户菜单
     * @param request 用户菜单信息
     * @return AResponse<Boolean>
     */
    @PostMapping("/saveUserMenu")
    @Log(clazz = "DaUserMenuController", method = "saveUserMenu")
    public AResponse<Boolean> saveUserMenu(@RequestBody SaveUserMenuRequest request) {
        Boolean bol = daUserMenuService.saveUserMenu(request);
        return ResultUtils.success(bol);
    }
}
