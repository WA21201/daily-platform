package com.wang.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.application.model.DaAuth;

/**
* @author Mr.wang
* @description 针对表【da_auth(权限表)】的数据库操作Mapper
* @createDate 2022-11-02 09:56:19
* @Entity com.wang.application.model.DaAuth
*/
public interface DaAuthMapper extends BaseMapper<DaAuth> {

}




