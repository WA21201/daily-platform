package com.wang.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.application.model.DaLogLevel;

/**
* @author Mr.wang
* @description 针对表【da_log_level(日志级别表)】的数据库操作Mapper
* @createDate 2022-11-02 10:01:04
* @Entity com.wang.application.model.DaLogLevel
*/
public interface DaLogLevelMapper extends BaseMapper<DaLogLevel> {

}




