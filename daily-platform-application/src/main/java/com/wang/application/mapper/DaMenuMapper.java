package com.wang.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.application.api.userMenu.queryUserMenu.LeftMenuList;
import com.wang.application.api.userMenu.queryUserMenu.MenuFatherList;
import com.wang.application.api.userMenu.queryUserMenu.UserMenuList;
import com.wang.application.model.DaMenu;

import java.util.List;
import java.util.Map;

/**
* @author Mr.wang
* @description 针对表【da_menu(菜单表)】的数据库操作Mapper
* @createDate 2022-11-02 10:01:09
* @Entity com.wang.application.model.DaMenu
*/
public interface DaMenuMapper extends BaseMapper<DaMenu> {

    /**
     * 查询父级菜单
     */
    List<MenuFatherList> selectFatherMenu(Map<String, String> map);

    /**
     * 用户菜单查询
     */
    List<UserMenuList> selectUserMenu(Map<String, String> map);

    /**
     * 左侧菜单查询
     */
    List<LeftMenuList> selectLeftMenu(Map<String, String> map);
}




