package com.wang.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.application.model.DaProblem;

/**
* @author Mr.wang
* @description 针对表【da_problem(问题表)】的数据库操作Mapper
* @createDate 2022-11-08 08:58:29
* @Entity com.wang.application.model.DaProblem
*/
public interface DaProblemMapper extends BaseMapper<DaProblem> {

}




