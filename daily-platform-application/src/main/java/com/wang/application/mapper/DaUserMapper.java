package com.wang.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.application.model.DaUser;

/**
* @author Mr.wang
* @description 针对表【da_user(用户表)】的数据库操作Mapper
* @createDate 2022-11-02 10:01:21
* @Entity com.wang.application.model.DaUser
*/
public interface DaUserMapper extends BaseMapper<DaUser> {

    /**
     * 修改用户信息
     */
    int updateUserInfo(DaUser daUser);

}




