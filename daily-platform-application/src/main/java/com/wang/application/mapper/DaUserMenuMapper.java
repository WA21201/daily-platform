package com.wang.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.application.model.DaUserMenu;

/**
* @author Mr.wang
* @description 针对表【da_user_menu(用户菜单表)】的数据库操作Mapper
* @createDate 2022-11-02 10:01:25
* @Entity com.wang.application.model.DaUserMenu
*/
public interface DaUserMenuMapper extends BaseMapper<DaUserMenu> {

}




