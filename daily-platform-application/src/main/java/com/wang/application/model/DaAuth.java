package com.wang.application.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * 权限表
 * @TableName da_auth
 */
@Data
public class DaAuth implements Serializable {
    /**
     * 权限id
     */
    @TableId("authid")
    private String authid;

    /**
     * 权限名称
     */
    private String authname;

    /**
     * 权限描述
     */
    private String authdesc;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DaAuth other = (DaAuth) that;
        return (this.getAuthid() == null ? other.getAuthid() == null : this.getAuthid().equals(other.getAuthid()))
            && (this.getAuthname() == null ? other.getAuthname() == null : this.getAuthname().equals(other.getAuthname()))
            && (this.getAuthdesc() == null ? other.getAuthdesc() == null : this.getAuthdesc().equals(other.getAuthdesc()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAuthid() == null) ? 0 : getAuthid().hashCode());
        result = prime * result + ((getAuthname() == null) ? 0 : getAuthname().hashCode());
        result = prime * result + ((getAuthdesc() == null) ? 0 : getAuthdesc().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", authid=").append(authid);
        sb.append(", authname=").append(authname);
        sb.append(", authdesc=").append(authdesc);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}