package com.wang.application.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * 菜单表
 * @TableName da_menu
 */
@Data
public class DaMenu implements Serializable {
    /**
     * 菜单id
     */
    @TableId("menuid")
    private String menuid;

    /**
     * 菜单码
     */
    private String menucode;

    /**
     * 父级菜单id,0表示为父级
     */
    private String fatherid;

    /**
     * 菜单名
     */
    private String menuname;

    /**
     * 添加日期
     */
    private String createtime;

    /**
     * 是否激活 0：否，1：是，默认1
     */
    private String status;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DaMenu other = (DaMenu) that;
        return (this.getMenuid() == null ? other.getMenuid() == null : this.getMenuid().equals(other.getMenuid()))
            && (this.getMenucode() == null ? other.getMenucode() == null : this.getMenucode().equals(other.getMenucode()))
            && (this.getFatherid() == null ? other.getFatherid() == null : this.getFatherid().equals(other.getFatherid()))
            && (this.getMenuname() == null ? other.getMenuname() == null : this.getMenuname().equals(other.getMenuname()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getMenuid() == null) ? 0 : getMenuid().hashCode());
        result = prime * result + ((getMenucode() == null) ? 0 : getMenucode().hashCode());
        result = prime * result + ((getFatherid() == null) ? 0 : getFatherid().hashCode());
        result = prime * result + ((getMenuname() == null) ? 0 : getMenuname().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", menuid=").append(menuid);
        sb.append(", menucode=").append(menucode);
        sb.append(", fatherid=").append(fatherid);
        sb.append(", menuname=").append(menuname);
        sb.append(", createtime=").append(createtime);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}