package com.wang.application.model;

import com.baomidou.mybatisplus.annotation.TableId;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * 问题表
 * @TableName da_problem
 */
@XmlRootElement(name = "DaProblem")
@XmlAccessorType(XmlAccessType.FIELD)
public class DaProblem implements Serializable {
    /**
     * 问题id
     */
    @TableId("problemid")
    private String problemid;

    /**
     * 问题
     */
    private String problem;

    private static final long serialVersionUID = 1L;

    /**
     * 问题id
     */
    public String getProblemid() {
        return problemid;
    }

    /**
     * 问题id
     */
    public void setProblemid(String problemid) {
        this.problemid = problemid;
    }

    /**
     * 问题
     */
    public String getProblem() {
        return problem;
    }

    /**
     * 问题
     */
    public void setProblem(String problem) {
        this.problem = problem;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DaProblem other = (DaProblem) that;
        return (this.getProblemid() == null ? other.getProblemid() == null : this.getProblemid().equals(other.getProblemid()))
            && (this.getProblem() == null ? other.getProblem() == null : this.getProblem().equals(other.getProblem()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProblemid() == null) ? 0 : getProblemid().hashCode());
        result = prime * result + ((getProblem() == null) ? 0 : getProblem().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", problemid=").append(problemid);
        sb.append(", problem=").append(problem);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}