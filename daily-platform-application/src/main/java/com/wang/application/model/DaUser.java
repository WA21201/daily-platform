package com.wang.application.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * 用户表
 *
 * @TableName da_user
 */
@Data
@XmlRootElement(name = "DaUser")
@XmlAccessorType(XmlAccessType.FIELD)
public class DaUser implements Serializable {
    /**
     * 用户id
     */
    @TableId("userid")
    private String userid;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 权限id
     */
    private String authid;

    /**
     * 头像
     */
    private String headimage;

    /**
     * 创建时间
     */
    private String createtime;

    /**
     * 是否激活，0：否，1：是，默认1
     */
    private String status;

    /**
     * 问题1
     */
    private String problem1;

    /**
     * 答案1
     */
    private String answer1;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DaUser other = (DaUser) that;
        return (this.getUserid() == null ? other.getUserid() == null : this.getUserid().equals(other.getUserid()))
                && (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
                && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
                && (this.getNickname() == null ? other.getNickname() == null : this.getNickname().equals(other.getNickname()))
                && (this.getAuthid() == null ? other.getAuthid() == null : this.getAuthid().equals(other.getAuthid()))
                && (this.getHeadimage() == null ? other.getHeadimage() == null : this.getHeadimage().equals(other.getHeadimage()))
                && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
                && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
                && (this.getProblem1() == null ? other.getProblem1() == null : this.getProblem1().equals(other.getProblem1()))
                && (this.getAnswer1() == null ? other.getAnswer1() == null : this.getAnswer1().equals(other.getAnswer1()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUserid() == null) ? 0 : getUserid().hashCode());
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getNickname() == null) ? 0 : getNickname().hashCode());
        result = prime * result + ((getAuthid() == null) ? 0 : getAuthid().hashCode());
        result = prime * result + ((getHeadimage() == null) ? 0 : getHeadimage().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getProblem1() == null) ? 0 : getProblem1().hashCode());
        result = prime * result + ((getAnswer1() == null) ? 0 : getAnswer1().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userid=").append(userid);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", nickname=").append(nickname);
        sb.append(", authid=").append(authid);
        sb.append(", headimage=").append(headimage);
        sb.append(", createtime=").append(createtime);
        sb.append(", status=").append(status);
        sb.append(", problem1=").append(problem1);
        sb.append(", answer1=").append(answer1);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}