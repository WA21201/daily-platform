package com.wang.application.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * 用户菜单表
 * @TableName da_user_menu
 */
@Data
@XmlRootElement(name = "DaUserMenu")
@XmlAccessorType(XmlAccessType.FIELD)
public class DaUserMenu implements Serializable {
    /**
     * 用户菜单id
     */
    @TableId("usermenuid")
    private String usermenuid;

    /**
     * 用户id
     */
    private String userid;

    /**
     * 菜单id
     */
    private String menuid;

    /**
     * 是否激活，0：否，1：是，默认1
     */
    private String status;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DaUserMenu other = (DaUserMenu) that;
        return (this.getUsermenuid() == null ? other.getUsermenuid() == null : this.getUsermenuid().equals(other.getUsermenuid()))
            && (this.getUserid() == null ? other.getUserid() == null : this.getUserid().equals(other.getUserid()))
            && (this.getMenuid() == null ? other.getMenuid() == null : this.getMenuid().equals(other.getMenuid()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUsermenuid() == null) ? 0 : getUsermenuid().hashCode());
        result = prime * result + ((getUserid() == null) ? 0 : getUserid().hashCode());
        result = prime * result + ((getMenuid() == null) ? 0 : getMenuid().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", usermenuid=").append(usermenuid);
        sb.append(", userid=").append(userid);
        sb.append(", menuid=").append(menuid);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}