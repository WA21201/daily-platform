package com.wang.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.application.model.DaAuth;

/**
* @author Mr.wang
* @description 针对表【da_auth(权限表)】的数据库操作Service
* @createDate 2022-11-02 09:56:19
*/
public interface DaAuthService extends IService<DaAuth> {

}
