package com.wang.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.application.model.DaLogLevel;

/**
* @author Mr.wang
* @description 针对表【da_log_level(日志级别表)】的数据库操作Service
* @createDate 2022-11-02 10:01:04
*/
public interface DaLogLevelService extends IService<DaLogLevel> {

}
