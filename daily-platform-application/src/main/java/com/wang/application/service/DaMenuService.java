package com.wang.application.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.application.api.menu.deleteMenu.DeleteMenuRequest;
import com.wang.application.api.menu.queryAllMenu.QueryAllMenuRequest;
import com.wang.application.api.menu.saveMenu.SaveMenuRequest;
import com.wang.application.model.DaMenu;

/**
* @author Mr.wang
* @description 针对表【da_menu(菜单表)】的数据库操作Service
* @createDate 2022-11-02 10:01:09
*/
public interface DaMenuService extends IService<DaMenu> {

    /**
     * 查询所有菜单
     */
    Page<DaMenu> queryAllMenu(QueryAllMenuRequest request);

    /**
     * 新增/保存菜单
     */
    Boolean saveMenu(SaveMenuRequest request);

    /**
     * 删除菜单
     */
    Boolean deleteMenu(DeleteMenuRequest request);
}
