package com.wang.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.application.model.DaProblem;

import java.util.List;

/**
* @author Mr.wang
* @description 针对表【da_problem(问题表)】的数据库操作Service
* @createDate 2022-11-08 08:58:29
*/
public interface DaProblemService extends IService<DaProblem> {

    /**
     * 获取问题列表
     */
    List<DaProblem> queryProblems();

}
