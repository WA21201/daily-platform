package com.wang.application.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.application.api.userMenu.queryUserMenu.AllUserList;
import com.wang.application.api.userMenu.queryUserMenu.QueryAllUserMenuRequest;
import com.wang.application.api.userMenu.queryUserMenu.QueryUserMenuResponse;
import com.wang.application.api.userMenu.saveUserMenu.SaveUserMenuRequest;
import com.wang.application.model.DaUser;
import com.wang.application.model.DaUserMenu;

/**
* @author Mr.wang
* @description 针对表【da_user_menu(用户菜单表)】的数据库操作Service
* @createDate 2022-11-02 10:01:25
*/
public interface DaUserMenuService extends IService<DaUserMenu> {

    /**
     * 当前用户菜单查询
     */
    QueryUserMenuResponse queryUserMenu();

    /**
     * 查询所有用户菜单分页
     */
    Page<AllUserList> queryAllUserMenu(QueryAllUserMenuRequest request);

    /**
     * 新增/保存用户菜单
     */
    Boolean saveUserMenu(SaveUserMenuRequest request);

}
