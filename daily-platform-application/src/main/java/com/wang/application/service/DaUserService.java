package com.wang.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.application.api.user.LoginRequest;
import com.wang.application.api.user.LoginResponse;
import com.wang.application.api.user.RegisterRequest;
import com.wang.application.api.user.UpdateRequest;
import com.wang.application.model.DaUser;
import org.springframework.web.multipart.MultipartFile;

/**
* @author Mr.wang
* @description 针对表【da_user(用户表)】的数据库操作Service
* @createDate 2022-11-02 10:01:21
*/
public interface DaUserService extends IService<DaUser> {

    /**
     * 用户注册
     */
    Boolean register(RegisterRequest request);

    /**
     * 用户登录
     */
    LoginResponse login(LoginRequest request);

    /**
     * 用户登出
     */
    Boolean logout();

    /**
     * 头像上传
     */
    DaUser uploadHeadImage(MultipartFile file);

    /**
     * 修改个人信息
     */
    DaUser updateUserInfo(UpdateRequest request);

    /**
     * 忘记密码
     */
    Boolean forgetPassword(UpdateRequest request);

}
