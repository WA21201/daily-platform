package com.wang.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.application.mapper.DaAuthMapper;
import com.wang.application.model.DaAuth;
import com.wang.application.service.DaAuthService;
import org.springframework.stereotype.Service;

/**
* @author Mr.wang
* @description 针对表【da_auth(权限表)】的数据库操作Service实现
* @createDate 2022-11-02 09:56:19
*/
@Service
public class DaAuthServiceImpl extends ServiceImpl<DaAuthMapper, DaAuth>
    implements DaAuthService{

}




