package com.wang.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.application.mapper.DaLogLevelMapper;
import com.wang.application.model.DaLogLevel;
import com.wang.application.service.DaLogLevelService;
import org.springframework.stereotype.Service;

/**
* @author Mr.wang
* @description 针对表【da_log_level(日志级别表)】的数据库操作Service实现
* @createDate 2022-11-02 10:01:04
*/
@Service
public class DaLogLevelServiceImpl extends ServiceImpl<DaLogLevelMapper, DaLogLevel>
    implements DaLogLevelService{

}




