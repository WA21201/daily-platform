package com.wang.application.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.application.mapper.DaProblemMapper;
import com.wang.application.model.DaProblem;
import com.wang.application.service.DaProblemService;
import com.wang.common.common.ErrorCode;
import com.wang.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Mr.wang
* @description 针对表【da_problem(问题表)】的数据库操作Service实现
* @createDate 2022-11-08 08:58:29
*/
@Service
@Slf4j
public class DaProblemServiceImpl extends ServiceImpl<DaProblemMapper, DaProblem>
    implements DaProblemService{

    @Resource
    private DaProblemMapper daProblemMapper;

    @Override
    public List<DaProblem> queryProblems() {
        log.info("=======================queryProblems start");

        List<DaProblem> daProblems;
        QueryWrapper<DaProblem> problemQueryWrapper = new QueryWrapper<>();
        try {
            daProblems = daProblemMapper.selectList(problemQueryWrapper);
        } catch (Exception e) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "查询问题列表失败");
        }
        return daProblems;
    }
}




