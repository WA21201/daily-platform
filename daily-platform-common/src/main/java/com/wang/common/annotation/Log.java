package com.wang.common.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * 操作类
     */
    String clazz();

    /**
     * 操作方法
     */
    String method();

    /**
     * 日志级别id
     */
    String level() default "00000";
}
