package com.wang.common.common;


public abstract class ARequest<T> {

    /**
     * 请求类转换为实体类
     */
    public abstract T transfer();
}
