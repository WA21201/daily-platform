package com.wang.common.common;

import lombok.Data;

import java.util.Map;

@Data
public class AResponse<T> {

    private Integer code;

    private T data;

    private Map<String, String> error;

    private String message;

    private String description;

    public AResponse(){}

    public AResponse(Integer code, T data, Map<String, String> error, String message, String description) {
        this.code = code;
        this.data = data;
        this.error = error;
        this.message = message;
        this.description = description;
    }

    public AResponse(Integer code, T data, String message) {
        this(code, data, null, message, "");
    }

    public AResponse(Integer code, T data) {
        this(code, data, null, "", "");
    }

    public AResponse(Integer code, String message, Map<String, String> error) {
        this(code, null, error, message, "");
    }

    public AResponse(ErrorCode errorCode) {
        this(errorCode.getCode(), null, null, errorCode.getMessage(), errorCode.getDescription());
    }
}
