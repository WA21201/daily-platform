package com.wang.common.common;

import com.wang.common.exception.BusinessException;
import com.wang.common.model.RUser;
import com.wang.common.utils.Contact;
import com.wang.common.utils.RedisUtil;
import com.wang.common.utils.Utils;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 业务公共参数
 *
 * @Scope注解是Spring IOC 容器中的一个作用域，在Spring IOC容器中，它用来配置Bean实例的作用域对象。
 * @Scope具有以下几种作用域：
 * 1、singleton  单实例的（单例）（默认）    ----全局有且仅有一个实例
 * 2、prototype  多实例（多例）     ----每次获取Bean时候会有一个新的实例
 * 3、request    同一次请求   ----每次HTTP请求都会产生一个新的bean，同时该bean仅在当前HTTP request有效
 * 4、session   同一个会话级别     ----每次HTTP请求都会产生一个新的bean，同时该bean仅在当前HTTP session有效
 * @注意： Spring只帮我们管理单例模式Bean的完整生命周期，对于prototype的bean，Spring在创建好交给使用者之后则不会再管理后续的生命周期
 */
@Data
@Component("aService")
@Scope("prototype")
public class AService {

    @Resource
    protected HttpServletRequest req;

    @Resource
    protected HttpServletResponse res;

    @Resource
    protected RedisUtil redisUtil;

    // 头像访问前缀
    String headPrefix = "/file/";

    /**
     * 用户权限校验
     * @exception BusinessException
     */
    public void authValid() {
        String token = req.getHeader("token");
        RUser rUser = (RUser) Utils.jsonStringToObj(redisUtil.getString(token), RUser.class);
        if (!Contact.Auth.MAXAUTH.equals(rUser.getAuthid()))
            throw new BusinessException(ErrorCode.NO_AUTH, "没有权限操作");
    }
}
