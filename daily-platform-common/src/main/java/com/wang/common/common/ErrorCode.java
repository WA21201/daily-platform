package com.wang.common.common;

/**
 * 错误码
 */
public enum ErrorCode {

    SUCCESS(200, "操作成功", ""),
    PARAMS_ERROR(410, "请求参数错误", ""),
    NULL_ERROR(420, "请求参数为空", ""),
    NO_AUTH(430, "无权限", ""),
    NO_LOGIN(440, "未登录", ""),
    LOGIN_TIMEOUT(441, "登录超时", ""),
    NO_MENU(401, "用户无菜单权限", ""),
    SYSTEM_ERROR(510, "系统内部错误", "");

    /**
     * 状态码
     */
    private final Integer code;

    /**
     * 状态码信息
     */
    private final String message;

    /**
     * 详细描述
     */
    private final String description;

    ErrorCode(Integer code, String message, String description) {
        this.code = code;
        this.message = message;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
