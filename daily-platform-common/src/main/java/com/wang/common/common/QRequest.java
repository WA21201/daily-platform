package com.wang.common.common;

import lombok.Data;

import java.util.List;

/**
 * 查询请求体抽象方法类
 */
@Data
public abstract class QRequest<T> {

    protected long current = 1;

    protected long size;

    /** 排序 0：降序 1：升序 */
    protected String sort = "0";

    protected List<String> timeRange;

    protected String starttime;

    protected String endtime;

    /**
     * 查询请求体 转 相等查询条件类
     */
    public abstract T equalTransfer();

    /**
     * 查询请求体 转 相似查询条件类
     */
    public abstract T likeTransfer();
}
