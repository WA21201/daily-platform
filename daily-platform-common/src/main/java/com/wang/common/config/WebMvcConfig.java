package com.wang.common.config;

import com.wang.common.interceptor.LoginInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${prop.file.read}")
    private String readFile;

    @Value("${prop.file.write}")
    private String writeFile;

    @Value("${prop.file.pattern}")
    private String pattern;

    /**
     * 注入Bean
     */
    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

    /**
     * 添加请求拦截器
     * @param registry 注册
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor())
                // 拦截路径，所有请求都会被拦截，包括静态资源
                .addPathPatterns("/**")
                // 不包括的路径
                .excludePathPatterns("/user/register",
                        "/user/login",
                        "/user/forgetPassword",
                        "/problem/queryProblem",
                        "/home", "/static/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        setResource(registry, pattern, readFile);
        setResource(registry, pattern, writeFile);
    }

    private void setResource(ResourceHandlerRegistry registry, String pattern, String path) {
        if (StringUtils.hasText(pattern) &&
                StringUtils.hasText(path)) {
            if (!pattern.trim().startsWith("/")) {
                pattern = "/" + pattern.trim();
            }
            if (!path.trim().startsWith("/")) {
                path = "/" + path.trim();
            }
            if (!path.trim().endsWith("/")) {
                path = path.trim() + "/";
            }
            File pathFile = new File(path);
            if (!pathFile.exists()) {
                pathFile.mkdirs();
            }
            log.info("**************add resource path:{}****************", path);
            registry.addResourceHandler(pattern)
                    .addResourceLocations("file:" + path);
        }
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //设置允许跨域的路径
        registry.addMapping("/**")
                //设置允许跨域请求的域名
                //当**Credentials为true时，**Origin不能为星号，需为具体的ip地址【如果接口不带cookie,ip无需设成具体ip】
//                .allowedOrigins("*")
                .allowedOriginPatterns("*")
                //是否允许证书 不再默认开启
                .allowCredentials(true)
                //设置允许的方法
                .allowedMethods("*")
                //跨域允许时间
                .maxAge(3600);
    }
}
