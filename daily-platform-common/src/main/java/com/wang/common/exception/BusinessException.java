package com.wang.common.exception;

import com.wang.common.common.ErrorCode;

/**
 * 自定义异常类
 * 服务异常
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 2668971222738112832L;

    private final Integer code;

    private final String description;

    public BusinessException(String message, Integer code, String description) {
        super(message);
        this.code = code;
        this.description = description;
    }

    public BusinessException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = errorCode.getDescription();
    }

    public BusinessException(ErrorCode errorCode, String description) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
