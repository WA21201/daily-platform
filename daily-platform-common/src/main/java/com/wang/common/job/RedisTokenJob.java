package com.wang.common.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * redis token job
 * 每天早上5点
 * 清空token
 */
@Component
@Slf4j
public class RedisTokenJob {

    @Scheduled(cron = "0 0 5 * * ? ")
    public void doRemoveToken() {
        log.info("\n=======================doRemoveToken:{}", "执行token定时任务");
    }
}
