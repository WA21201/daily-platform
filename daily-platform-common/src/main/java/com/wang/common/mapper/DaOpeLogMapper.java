package com.wang.common.mapper;

import com.wang.common.model.DaOpeLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Mr.wang
* @description 针对表【da_ope_log(操作日志表)】的数据库操作Mapper
* @createDate 2022-11-02 16:15:10
* @Entity com.wang.common.model.DaOpeLog
*/
public interface DaOpeLogMapper extends BaseMapper<DaOpeLog> {

}




