package com.wang.common.model;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

/**
 * 操作日志表
 * @TableName da_ope_log
 */
public class DaOpeLog implements Serializable {
    /**
     * 日志id
     */
    @TableId("logid")
    private String logid;

    /**
     * 操作员id
     */
    private String userid;

    /**
     * 操作类
     */
    private String clazz;

    /**
     * 操作方法
     */
    private String method;

    /**
     * 日志级别id
     */
    private String levelid;

    /**
     * 操作备注
     */
    private String remark;

    /**
     * 操作日期
     */
    private String createtime;

    private static final long serialVersionUID = 1L;

    /**
     * 日志id
     */
    public String getLogid() {
        return logid;
    }

    /**
     * 日志id
     */
    public void setLogid(String logid) {
        this.logid = logid;
    }

    /**
     * 操作员id
     */
    public String getUserid() {
        return userid;
    }

    /**
     * 操作员id
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * 操作类
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * 操作类
     */
    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    /**
     * 操作方法
     */
    public String getMethod() {
        return method;
    }

    /**
     * 操作方法
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * 日志级别id
     */
    public String getLevelid() {
        return levelid;
    }

    /**
     * 日志级别id
     */
    public void setLevelid(String levelid) {
        this.levelid = levelid;
    }

    /**
     * 操作备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 操作备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 操作日期
     */
    public String getCreatetime() {
        return createtime;
    }

    /**
     * 操作日期
     */
    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DaOpeLog other = (DaOpeLog) that;
        return (this.getLogid() == null ? other.getLogid() == null : this.getLogid().equals(other.getLogid()))
            && (this.getUserid() == null ? other.getUserid() == null : this.getUserid().equals(other.getUserid()))
            && (this.getClazz() == null ? other.getClazz() == null : this.getClazz().equals(other.getClazz()))
            && (this.getMethod() == null ? other.getMethod() == null : this.getMethod().equals(other.getMethod()))
            && (this.getLevelid() == null ? other.getLevelid() == null : this.getLevelid().equals(other.getLevelid()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getLogid() == null) ? 0 : getLogid().hashCode());
        result = prime * result + ((getUserid() == null) ? 0 : getUserid().hashCode());
        result = prime * result + ((getClazz() == null) ? 0 : getClazz().hashCode());
        result = prime * result + ((getMethod() == null) ? 0 : getMethod().hashCode());
        result = prime * result + ((getLevelid() == null) ? 0 : getLevelid().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", logid=").append(logid);
        sb.append(", userid=").append(userid);
        sb.append(", clazz=").append(clazz);
        sb.append(", method=").append(method);
        sb.append(", levelid=").append(levelid);
        sb.append(", remark=").append(remark);
        sb.append(", createtime=").append(createtime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}