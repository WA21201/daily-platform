package com.wang.common.model;

import lombok.Data;

@Data
public class RUser {

    /**
     * 用户id
     */
    private String userid;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 权限id
     */
    private String authid;

    /**
     * 头像
     */
    private String headimage;

    /**
     * 是否激活，0：否，1：是，默认1
     */
    private String status;

    /**
     * 问题1
     */
    private String problem1;

    /**
     * 答案1
     */
    private String answer1;
}
