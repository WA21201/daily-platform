package com.wang.common.service;

import com.wang.common.model.DaOpeLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Mr.wang
* @description 针对表【da_ope_log(操作日志表)】的数据库操作Service
* @createDate 2022-11-02 16:15:10
*/
public interface DaOpeLogService extends IService<DaOpeLog> {

}
