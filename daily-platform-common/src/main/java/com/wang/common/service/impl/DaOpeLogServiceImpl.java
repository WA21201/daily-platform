package com.wang.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.common.model.DaOpeLog;
import com.wang.common.service.DaOpeLogService;
import com.wang.common.mapper.DaOpeLogMapper;
import org.springframework.stereotype.Service;

/**
* @author Mr.wang
* @description 针对表【da_ope_log(操作日志表)】的数据库操作Service实现
* @createDate 2022-11-02 16:15:10
*/
@Service
public class DaOpeLogServiceImpl extends ServiceImpl<DaOpeLogMapper, DaOpeLog>
    implements DaOpeLogService{

}




