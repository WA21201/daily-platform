package com.wang.common.utils;

/**
 * Array数组工具类
 */
public class ArrayUtil {

    /**
     * 数组反转
     */
    public static <T> void reverse(T[] arr) {
        int len = arr.length, i = 0, j = len - 1;
        while (i < j) {
            T temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
            j--;
        }
    }
}
