package com.wang.common.utils;

/**
 * 公共字段类
 */
public class Contact {

    /*************************************用户权限*************************************/
    public static class Auth {
        public static final String MAXAUTH = "000"; //最高权限
        public static final String MINAUTH = "100"; //最低权限
    }

    /*************************************激活状态*************************************/
    public static class Status {
        public static final String ACTIVE = "1"; //激活
        public static final String LOSE = "0"; //失效
    }
}
