package com.wang.common.utils;

import javax.xml.bind.Marshaller;
import java.lang.reflect.Field;

/**
 * 实体类转xml 监听类
 */
public class MarshallerListener extends Marshaller.Listener {

    private static final String BLACK = "";

    @Override
    public void beforeMarshal(Object source) {
        super.beforeMarshal(source);
        Field[] fields = source.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                if (field.getType().equals(String.class) && field.get(source) == null) {
                    field.set(source, BLACK);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
