package com.wang.common.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;

/**
 * mybatis plus工具类
 */
@Slf4j
public class MybatisUtil{

    /**
     * 过滤空值相等查询
     * @param wrapper QueryWrapper
     * @param entity 查询对象
     */
    public static <T> void queryFilter(QueryWrapper<T> wrapper, Object entity) {
        Map<String, String> map = null;
        try {
            map = ClassUtil.getMap(entity);
        } catch (Exception e) {
            log.info("getMap非空键值对获取失败", e);
        }
        if(map == null || map.size() < 1) return;
        map.forEach(wrapper::eq);
    }

    /**
     * 过滤空值相似查询
     * @param wrapper QueryWrapper
     * @param entity 查询对象
     */
    public static <T> void queryLikeFilter(QueryWrapper<T> wrapper, Object entity) {
        Map<String, String> map = null;
        try {
            map = ClassUtil.getMap(entity);
        } catch (Exception e) {
            log.info("getMap非空键值对获取失败", e);
        }
        if(map == null || map.size() < 1) return;
        map.forEach(wrapper::like);
    }


    /**
     * 过滤空值更新
     * @param entity 更新对象
     * @return UpdateWrapper
     */
    public static <T> UpdateWrapper<T> updateFilter(T entity) {
        UpdateWrapper<T> wrapper = Wrappers.update();
        try {
            Field[] fields = entity.getClass().newInstance().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> {
                String name = field.getName();
                String fieldValue = (String) ClassUtil.getFieldValueByName(entity, name);
                if (Utils.isNotEmpty(fieldValue)) {
                    wrapper.set(name, fieldValue);
                }
            });
        } catch (InstantiationException | IllegalAccessException e) {
            log.info("updateFilter失败", e);
        }
        return wrapper;
    }
}


