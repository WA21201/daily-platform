package com.wang.common.utils;

import com.wang.common.common.AResponse;
import com.wang.common.common.ErrorCode;

import java.util.Map;

public class ResultUtils {

    /**
     * 成功
     */
    public static <T> AResponse<T> success(T data) {
        return new AResponse<>(ErrorCode.SUCCESS.getCode(), data, ErrorCode.SUCCESS.getMessage());
    }

    /**
     * 失败
     */
    public static AResponse<?> error(ErrorCode code) {
        return new AResponse<>(code);
    }

    public static AResponse<Map<String, String>> error(ErrorCode code, Map<String, String> error) {
        return new AResponse<>(code.getCode(), code.getMessage(), error);
    }

    public static AResponse<?> error(ErrorCode code, String message) {
        return new AResponse<>(code.getCode(), null, message);
    }

    public static AResponse<?> error(Integer code, String message, String description) {
        return new AResponse<>(code, null, null, message, description);
    }
}
