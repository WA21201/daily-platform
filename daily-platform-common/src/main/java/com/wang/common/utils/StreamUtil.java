package com.wang.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 流工具类
 */
@Slf4j
public class StreamUtil {

    /**
     * 关闭输入流
     * @param inputStreams 输入流
     */
    public static void closeInputStream(InputStream... inputStreams) {
        for (InputStream inputStream : inputStreams) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 关闭输出流
     * @param outputStreams 输出流
     */
    public static void closeOutputStream(OutputStream... outputStreams) {
        for (OutputStream outputStream : outputStreams) {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 删除临时文件
     * @param LFPath 临时文件路径
     */
    public static void deleteLFile(String LFPath) {
        File file1 = new File(LFPath);
        boolean delete = file1.delete();
        if (!delete) log.error("删除zip文件" + LFPath + "失败");
    }
}
