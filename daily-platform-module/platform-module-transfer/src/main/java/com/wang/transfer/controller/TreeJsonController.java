package com.wang.transfer.controller;

import com.wang.common.annotation.Log;
import com.wang.common.common.AResponse;
import com.wang.common.common.AbstractController;
import com.wang.common.common.ErrorCode;
import com.wang.common.exception.BusinessException;
import com.wang.common.utils.ResultUtils;
import com.wang.transfer.entity.TreeToJsonConfig;
import com.wang.transfer.service.TreeJsonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/treeJson")
public class TreeJsonController extends AbstractController {

    @Resource
    private TreeJsonService treeJsonService;

    /**
     * tree to json
     */
    @PostMapping(value = "/propertiesToJson")
    @Log(clazz = "TreeJsonController", method = "treeJson")
    public AResponse<String> treeJson(MultipartFile file) {

        if (file == null || file.isEmpty()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "文件为空");
        }
        TreeToJsonConfig config = new TreeToJsonConfig();
        config.setSymbol(request.getParameter("symbol"));
        config.setIndent(request.getParameter("indent"));
        String s = treeJsonService.proToJson(file, config);
        return ResultUtils.success(s);
    }

    @PostMapping(value = "/propertiesToJsons")
    @Log(clazz = "TreeJsonController", method = "treeJsons")
    public AResponse<List<String>> treeJsons(List<MultipartFile> files) {
        if (files == null || files.isEmpty()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "文件为空");
        }
        TreeToJsonConfig config = new TreeToJsonConfig();
        config.setSymbol(request.getParameter("symbol"));
        config.setIndent(request.getParameter("indent"));
        List<String> list = treeJsonService.proToJsons(files, config);
        return ResultUtils.success(list);
    }
}
