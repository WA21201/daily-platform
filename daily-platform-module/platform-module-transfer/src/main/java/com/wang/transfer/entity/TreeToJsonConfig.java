package com.wang.transfer.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * tree to json config配置实体类
 */
@Data
public class TreeToJsonConfig implements Serializable {

    private static final long serialVersionUID = -3446197368339911101L;
    /**
     * 配置key-value连接符号
     */
    private String symbol;

    /**
     * 配置缩进格式
     */
    private String indent;
}
