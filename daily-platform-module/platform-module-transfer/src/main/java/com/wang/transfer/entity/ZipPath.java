package com.wang.transfer.entity;

import lombok.Data;

import java.util.List;

/**
 * 压缩zip包中的文件路径
 */
@Data
public class ZipPath {

    private List<String> path;
}
