package com.wang.transfer.service;

import com.wang.transfer.entity.TreeToJsonConfig;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * tree to json
 */
public interface TreeJsonService {

    /**
     * properties to json
     * @param file 转换文件
     * @param config 转换配置
     * @return 转换后的文件地址
     */
    String proToJson(MultipartFile file, TreeToJsonConfig config);

    /**
     * 批量文件长传转换
     * @param files 文件列表
     * @param config 转换配置
     * @return 转换后的文件地址
     */
    List<String> proToJsons(List<MultipartFile> files, TreeToJsonConfig config);
}
