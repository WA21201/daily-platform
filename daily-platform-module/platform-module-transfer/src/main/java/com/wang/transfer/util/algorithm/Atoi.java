package com.wang.transfer.util.algorithm;

/**
 * 字符串转换整数
 */
public class Atoi {

    public static void main(String[] args) {
        String s = "   -42";
        int i = new Atoi().myAtoi(s);
        System.out.println(i);
    }

    /**
     * 执行结果：通过
     * 执行用时：1 ms, 在所有 Java 提交中击败了100.00%的用户
     * 内存消耗：41.7 MB, 在所有 Java 提交中击败了25.55%的用户
     * 通过测试用例：1084 / 1084
     */
    public int myAtoi(String s) {
        s = s.trim();
        int n = s.length();
        int ao = 0;
        boolean tag = true;//正负数判断
        boolean done = false;//是否加上符号位
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if ((c == '-' || c == '+') && i == 0) {
                tag = c != '-';
                continue;
            } else if (c < '0' || c > '9') {
                // 非数字， 退出
                if (!tag) ao = -1 * ao;
                done = true;
                return ao;
            }
            int cc = Integer.parseInt(String.valueOf(c));
            int num = tag ? ao : -1 * ao;
            if (!tag && (num < (Integer.MIN_VALUE / 10) || (num == (Integer.MIN_VALUE / 10) && cc > -(Integer.MIN_VALUE % 10)))) {
                return Integer.MIN_VALUE;
            } else if (tag && (num > (Integer.MAX_VALUE / 10) || (num == Integer.MAX_VALUE / 10 && cc > Integer.MAX_VALUE % 10))) {
                return Integer.MAX_VALUE;
            } else {
                ao = ao * 10 + cc;
            }
        }
        if (!tag && !done) {
            ao = -1 * ao;
        }
        return ao;
    }
}
