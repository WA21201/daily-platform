package com.wang.transfer.util.algorithm;

/**
 * 整数转罗马数字
 */
class IntToRoman {

    public static void main(String[] args) {
        int num = 1994;
        String s = new IntToRoman().intToRoman(num);
        System.out.println(s);
    }

    public String intToRoman(int num) {
        int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] symbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        StringBuilder roman = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            int value = values[i];
            while (num >= value) {
                num -= value;
                roman.append(symbols[i]);
            }
            if (num == 0) {
                break;
            }
        }
        return roman.toString();
    }
}
