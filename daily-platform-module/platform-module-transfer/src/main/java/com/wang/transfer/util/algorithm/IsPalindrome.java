package com.wang.transfer.util.algorithm;

/**
 * 整数是否是回文数
 */
public class IsPalindrome {

    public static void main(String[] args) {
        int x = 0;
        boolean palindrome = new IsPalindrome().isPalindrome(x);
        System.out.println(palindrome);
    }

    public boolean isPalindrome(int x) {
        String sx = String.valueOf(x);
        int n = sx.length();
        if (n == 1) {
            return true;
        }
        int i = 0, j = n - 1, num = 1;
        while(i <= j && sx.charAt(i) == sx.charAt(j)) {
            num *= 2;
            ++i;
            --j;
        }
        return num >= n && sx.charAt(i) == sx.charAt(j);
    }
}
