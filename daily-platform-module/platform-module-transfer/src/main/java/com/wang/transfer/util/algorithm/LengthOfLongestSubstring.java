package com.wang.transfer.util.algorithm;

import java.util.HashSet;
import java.util.Set;

/**
 * 最长子串的长度
 */
public class LengthOfLongestSubstring {

    public static void main(String[] args) {
        String s = "abcabcaaba";
        int i = new LengthOfLongestSubstring().lengthLongest(s);
        System.out.println(i);
    }

    public int lengthLongest(String s) {
        // 存放出现过的不同的字符
        Set<Character> set = new HashSet<>();
        // r 右指针 ans 记录最长子串长度
        int r = 0, ans = 0;
        int n = s.length();
        for (int i = 0; i < n; i++) {
            if (i != 0) {
                // 左指针向右移动，说明子串出现重复字符，移除开头的字符
                set.remove(s.charAt(i - 1));
            }
            while (r < n && !set.contains(s.charAt(r))) {
                set.add(s.charAt(r));
                r++;
            }
            ans = Math.max(ans, set.size());
        }
        return ans;
    }
}
