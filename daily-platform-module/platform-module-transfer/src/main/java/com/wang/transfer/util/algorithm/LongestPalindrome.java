package com.wang.transfer.util.algorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 最长回文子串
 */
public class LongestPalindrome {

    public static void main(String[] args) {
        String s = "asdfasgfdsgergfdgggsdfgs";
//        String s1 = new LongestPalindrome().LongestStr(s);
        String s1 = new LongestPalindrome().longMe(s);
        System.out.println(s1);
    }

    /**
     * 超时
     */
    public String LongestStr(String s) {
        String ans = "";
        int n = s.length();
        if (n == 1) {
            return s;
        }
        for (int i = 0; i < n; i++) {
            List<Character> str = new ArrayList<>();
            Set<Character> ch = new HashSet<>();
            for (int k = 0; k < s.length(); k++) {
                ch.add(s.charAt(k));
            }
            if (ch.size() == 1) return s;
            String sub = s.substring(i, i + 1);
            ans = ans.length() >= sub.length() ? ans : sub;
            str.add(s.charAt(i));
            for (int j = i + 1; j < n; j++) {
                str.add(s.charAt(j));
                boolean tag = true;
                for (int z = str.size() - 1; z >= str.size() / 2; z--) {
                    if (!str.get(z).equals(str.get(str.size() - 1 - z))) {
                        tag = false;
                        break;
                    }
                }
                if (tag) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (Character character : str) {
                        stringBuilder.append(character);
                    }
                    String s1 = stringBuilder.toString();
                    ans = ans.length() > s1.length() ? ans : s1;
                }
            }
        }
        return ans;
    }

    /**
     * author: 官方
     * dp[i][j] = dp[i + 1][j - 1]:大串是否是回文子串进一步根据它的子串决定
     */
    public String longestPalindrome(String s) {
        int len = s.length();
        if (len < 2) {
            return s;
        }

        int maxLen = 1;
        int begin = 0;
        // dp[i][j] 表示 s[i..j] 是否是回文串
        boolean[][] dp = new boolean[len][len];
        // 初始化：所有长度为 1 的子串都是回文串
        for (int i = 0; i < len; i++) {
            dp[i][i] = true;
        }

        char[] charArray = s.toCharArray();
        // 递推开始
        // 先枚举子串长度
        for (int L = 2; L <= len; L++) {
            // 枚举左边界，左边界的上限设置可以宽松一些
            for (int i = 0; i < len; i++) {
                // 由 L 和 i 可以确定右边界，即 j - i + 1 = L 得
                int j = L + i - 1;
                // 如果右边界越界，就可以退出当前循环
                if (j >= len) {
                    break;
                }

                if (charArray[i] != charArray[j]) {
                    dp[i][j] = false;
                } else {
                    if (j - i < 3) {
                        dp[i][j] = true;
                    } else {
                        dp[i][j] = dp[i + 1][j - 1];
                    }
                }

                // 只要 dp[i][L] == true 成立，就表示子串 s[i..L] 是回文，此时记录回文长度和起始位置
                if (dp[i][j] && j - i + 1 > maxLen) {
                    maxLen = j - i + 1;
                    begin = i;
                }
            }
        }
        return s.substring(begin, begin + maxLen);
    }

    public String longMe(String s) {
        int len = s.length();
        if (len < 2) {
            return s;
        }

        int maxLen = 1;
        int begin = 0;
        boolean[][] dp = new boolean[len][len];
        for (int i = 0; i < len; i++) {
            dp[i][i] = true;
        }

        char[] charArray = s.toCharArray();
        for (int L = 2; L <= len; L++) {
            for (int i = 0; i < len; i++) {
                int j = L + i -1;
                if (j >= len) {
                    break;
                }
                if (charArray[i] != charArray[j]) {
                    dp[i][j] = false;
                } else {
                    if (j - i < 3) {
                        dp[i][j] = true;
                    } else {
                        dp[i][j] = dp[i + 1][j - 1];
                    }
                }
                if (dp[i][j] && j -i + 1 > maxLen) {
                    maxLen = j -i + 1;
                    begin = i;
                }
            }
        }
        return s.substring(begin, begin + maxLen);
    }
}
