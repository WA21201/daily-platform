package com.wang.transfer.util.algorithm;

import javax.validation.constraints.Max;

/**
 * 整数数组，最大容量
 */
public class MaxArea {

    public static void main(String[] args) {
        int[] height = new int[]{1,8,6,2,5,4,8,3,7};
//        int i = new MaxArea().maxArea(height);
        int i = new MaxArea().maxArea2(height);
        System.out.println(i);
    }

    public int maxArea(int[] height) {
        int ma = 0;
        for (int i = 0; i < height.length; i++) {
            for (int j = i + 1; j < height.length; j++) {
                int he = Math.min(height[i], height[j]);
                ma = Math.max(ma, he * (j - i));
            }
        }
        return ma;
    }

    public int maxArea2(int[] height) {
        int ma = 0;
        int length = height.length;
        int i = 0, j = length - 1;
        while(i < j) {
            ma = Math.max(ma, Math.min(height[i], height[j]) * (j - i));
            if (height[i] <= height[j]) {
                i++;
            } else {
                j--;
            }
        }
        return ma;
    }
}
