package com.wang.transfer.util.algorithm;

import java.util.HashMap;
import java.util.Map;

/**
 * 罗马数字转数字
 */
class RomanToInt {

    public static void main(String[] args) {
        String s = "MCMXCIV";
        int i = new RomanToInt().romanToInt(s);
        System.out.println(i);
    }

    public int romanToInt(String s) {
        Map<String, Integer> map = new HashMap<>();
        map.put("M", 1000);
        map.put("CM", 900);
        map.put("D", 500);
        map.put("CD", 400);
        map.put("C", 100);
        map.put("XC", 90);
        map.put("L", 50);
        map.put("XL", 40);
        map.put("X", 10);
        map.put("IX", 9);
        map.put("V", 5);
        map.put("IV", 4);
        map.put("I", 1);

        int num = 0;
        while (s.length() > 0) {
            String sub = s.length() > 1 && map.containsKey(s.substring(0, 2)) ? s.substring(0, 2) : s.substring(0, 1);
            num += map.get(sub);
            s = s.substring(sub.length());
        }
        return num;
    }
}
