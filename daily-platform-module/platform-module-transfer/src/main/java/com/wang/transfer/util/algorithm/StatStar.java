package com.wang.transfer.util.algorithm;

import java.nio.charset.StandardCharsets;

/**
 * 统计星号
 */
public class StatStar {

    public static void main(String[] args) {
        String s = "yo|uar|e**|b|e***au|tifu|l";

        int num = 0;
//        String[] ls = s.split("\\|");
//        for (int i = 0; i < ls.length; i += 2) {
//            for (int j = 0; j < ls[i].length() ; j++ ) {
//                char c = ls[i].charAt(j);
//                if ('*' == c) {
//                    num ++;
//                }
//            }
//        }
        // 奇数|之前，偶数|之后的*的个数
        boolean valid = true;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if ('|' == c) {
                valid = !valid;
            } else if ('*' == c && valid) {
                num++;
            }
        }
        System.out.println(num);
    }
}
