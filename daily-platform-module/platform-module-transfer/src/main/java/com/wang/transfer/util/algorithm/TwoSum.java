package com.wang.transfer.util.algorithm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 两数之和 获取下标
 */
public class TwoSum {

    public static void main(String[] args) {
        int[] nums = {3,2,4};
        int target = 6;

//        Arrays.sort(nums);

//        for (int i = 0; i < nums.length; ++i) {
//            for (int j = i + 1; j <  nums.length; ++j) {
//                if (nums[i] + nums[j] == target) {
//                    System.out.println(Arrays.toString(new int[]{i, j}));
//                    return;
//                }
//            }
//        }
//        System.out.println(Arrays.toString(new int[0]));

        // 哈希表
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; ++i) {
            if (map.containsKey(target - nums[i])) {
                System.out.println(Arrays.toString(new int[]{ map.get(target - nums[i]), i}));
                return;
            }
            map.put(nums[i], i);
        }
        System.out.println(Arrays.toString(new int[0]));
    }
}
