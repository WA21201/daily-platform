package com.wang.transfer.util.io;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * 管道流，用于网络编程中
 */
public class FileBuffered {

    public static void main(String[] args) {
        InputStream io = new BufferedInputStream(
                new ByteArrayInputStream(
                        "王双宝".getBytes(StandardCharsets.UTF_8)
                )
        );

        // 操作
        byte[] flush = new byte[1024];
        int len;
        try {
            while (-1 != (len = io.read(flush))) {
                System.out.println(new String(flush, 0, len));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                io.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
