package com.wang.transfer.util.io;

import com.alibaba.fastjson2.JSONObject;
import com.wang.common.common.AService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * servlet的response流返回json或文件
 */
@Service
@Slf4j
public class ServletResponse extends AService {

    @Resource
    private DifferentFileBuffered differentFileBuffered;

    public void doGet(String dataType, String filePath) throws IOException {
        log.info("\n=======================doGet:{}", dataType + "||" + filePath);
        if ("json".equals(dataType)) {
            // 返回json格式数据
            res.setContentType("application/json;charset=utf-8");
            res.setCharacterEncoding("utf-8");
            String str = "{\"reciptNo\":\"NR00001\"}";
            Map<String, String> map = new HashMap<>();
            map.put("reciptNo", "NR002342");
            map.put("filePath", "/34234/2342/");
            map.put("fileNm", "sdfsdf.pdf");
            ServletOutputStream out = res.getOutputStream();
            out.println(JSONObject.toJSONString(map));
            out.flush();
            out.close();
            return;
        }
        differentFileBuffered.getFileStream(filePath, DifferentFileBuffered.ContentType.MSDOWNLOAD);
    }
}
