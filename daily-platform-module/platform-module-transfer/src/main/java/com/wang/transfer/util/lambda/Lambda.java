package com.wang.transfer.util.lambda;

import java.util.*;
import java.util.stream.Collectors;

public class Lambda {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("apple", "orange", "banana");

        list.sort((String::compareTo));
        list.forEach(System.out::println);

        List<String> list1 = list.stream().filter(l -> l.startsWith("a")).collect(Collectors.toList());
        List<Integer> list2 = list.stream().map(String::length).collect(Collectors.toList());
        Map<Integer, List<String>> group = list.stream().collect(Collectors.groupingBy(String::length));
        System.out.println(group);
        List<String> list4 = list.stream().filter(s -> s.startsWith("a")).map(String::toUpperCase).sorted().collect(Collectors.toList());


        List<Integer> list3 = Arrays.asList(1,2,3,4,5,6);
        int sum = list3.stream().reduce(0, Integer::sum);
        System.out.println(sum);

        MyInterface myInterface1 = System.out::println;
        myInterface1.doSomething("hello world");

        Thread thread = new Thread(() -> System.out.println("thread"));
        thread.start();

        String str = "hello world";
        Optional.ofNullable(str).map(String::toUpperCase).ifPresent(System.out::println);
    }
}

interface MyInterface {
    public void doSomething(String s);
}
