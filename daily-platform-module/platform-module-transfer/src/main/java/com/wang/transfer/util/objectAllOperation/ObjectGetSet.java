package com.wang.transfer.util.objectAllOperation;

import com.wang.common.utils.ClassUtil;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

/**
 * <code>proxy.set...</code> <code>request.get...</code> <code>ret.set...</code> <code>response.get...</code> <code>listArr.set...</code> <code>arr.get...</code>
 *
 * @author Mr.wang
 * @since 2022.9.20
 */
public class ObjectGetSet {

    private static final String OPERATION_EXIT = "EXIT";

    private static final String TARGET = "/objectGetSetTarget";

    private static String CLASSNAME = "";

    private static final String selfSet = "cbFlowTradeFinancing";

    private static final String selfGet = "request";

    private static final ObjectGetSet _this = new ObjectGetSet();

    public static void main(String[] args) {

        _this.autoExecute();

        /*
          程序保持持续运行
         */
        System.out.println("请输入类名，EXIT/E 退出");
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            String in = scan.next();
            if (OPERATION_EXIT.equalsIgnoreCase(in)
                    || OPERATION_EXIT.substring(0, 1).equalsIgnoreCase(in)) {
                System.out.println("您已成功退出！");
                break;
            }
            System.out.println("======" + in + "======");
            CLASSNAME = in;
            _this.execute("");
            System.out.println("======结束======");
        }
    }

    /**
     * autoExecute
     */
    private void autoExecute() {
        String pkg = ObjectGetSet.class.getPackage().getName();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        String packagePath = pkg.replace(".", "/") + TARGET;
        URL url = loader.getResource(packagePath);
        if (url != null) {
            String type = url.getProtocol();
            if ("file".equals(type)) {
                String path = url.getPath();
                path = path.substring(0, path.lastIndexOf(TARGET) + TARGET.length());
                List<String> classNameList = ClassUtil.getClassNameByPackage(path);
                for (String s : classNameList) {
                    System.out.println("======" + s + "======");
                    execute(s);
                    System.out.println("======结束======");
                }
            }
        }
    }

    /**
     * execute
     */
    private void execute(String clz) {
        String pkg = ObjectGetSet.class.getPackage().getName();
        if ("".equals(clz) || clz == null) {
            clz = pkg + "." + TARGET.substring(1) + "." + CLASSNAME;
        }
        Class<?> cls;
        try {
            cls = Class.forName(clz);
            Field[] fields = cls.newInstance().getClass().getDeclaredFields();
            if (CLASSNAME.endsWith("Request") || clz.endsWith("Request")) {
                proxyRequest(fields);
            } else if (CLASSNAME.endsWith("Response") || clz.endsWith("Response")) {
                retResponse(fields);
            } else if (CLASSNAME.endsWith("List") || clz.endsWith("List")) {
                listArr(fields);
            } else {
                self(fields);
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            System.out.println("=== 【类未找到】 ===");
        }
    }

    /**
     * proxy.set
     * request.get
     */
    private void proxyRequest(Field[] fields) {
        for (Field field : fields) {
            String reqName = field.getName();
            if ("SerialVersionUID".equalsIgnoreCase(reqName)) continue;
            reqName = reqName.substring(0, 1).toUpperCase() + reqName.substring(1);
            String code = "proxy.set" + reqName + "(request.get" + reqName + "());";
            System.out.println(code);
        }
    }

    /**
     * ret.set
     * response.get
     */
    private void retResponse(Field[] fields) {
        for (Field field : fields) {
            String reqName = field.getName();
            if ("SerialVersionUID".equalsIgnoreCase(reqName)) continue;
            reqName = reqName.substring(0, 1).toUpperCase() + reqName.substring(1);
            String code = "res.set" + reqName + "(response.get" + reqName + "());";
            System.out.println(code);
        }
    }

    /**
     * listArr.set
     * arr.get
     */
    private void listArr(Field[] fields) {
        for (Field field : fields) {
            String reqName = field.getName();
            reqName = reqName.substring(0, 1).toUpperCase() + reqName.substring(1);
            String code = "listArr.set" + reqName + "(arr.get" + reqName + "());";
            System.out.println(code);
        }
    }

    /**
     * self
     * ....set
     * ....get
     */
    private void self(Field[] fields) {
        for (Field field : fields) {
            String reqName = field.getName();
            reqName = reqName.substring(0, 1).toUpperCase() + reqName.substring(1);
            String code = selfSet + ".set" + reqName + "(" + selfGet + ".get" + reqName + "());";
            System.out.println(code);
        }
    }
}
