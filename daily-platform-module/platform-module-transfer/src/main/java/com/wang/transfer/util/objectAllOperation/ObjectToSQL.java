package com.wang.transfer.util.objectAllOperation;

import com.wang.common.utils.ArrayUtil;
import com.wang.common.utils.ClassUtil;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * object to sql
 * @author Mr.wang
 * @since 2022.10.24
 */
@Slf4j
public class ObjectToSQL {

    // 退出程序指定指令
    private static final String OPERATION_EXIT = "EXIT";

    // 指定包下的类
    private static final String TARGET = "/objectGetSetTarget";

    // 数据库表名
    private static final String DBNAME = "cb_flow_guarantee";

    // 输入的指定类名
    private static String CLASSNAME = "";

    private static final ObjectToSQL _this = new ObjectToSQL();

    public static void main(String[] args) {

        _this.autoExecute();

        /**
         * 程序保持持续运行
         */
        System.out.println("请输入类名，EXIT/E 退出");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String in = scanner.next();
            if (OPERATION_EXIT.equalsIgnoreCase(in)
                    || OPERATION_EXIT.substring(0, 1).equalsIgnoreCase(in)) {
                System.out.println("您已成功退出！");
                break;
            }
            System.out.println("==============" + in + "=============");
            CLASSNAME = in;
            _this.execute("");
            System.out.println("==============结束=============");
        }
    }

    /**
     * autoExecute
     */
    private void autoExecute() {
        String pkg = ObjectToSQL.class.getPackage().getName();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        String packagePath = pkg.replace(".", "/") + TARGET;
        URL url = loader.getResource(packagePath);
        if (url != null) {
            String type = url.getProtocol();
            if ("file".equals(type)) {
                String path = url.getPath();
                path = path.substring(0, path.lastIndexOf(TARGET) + TARGET.length());
                List<String> classNameList = ClassUtil.getClassNameByPackage(path);
                for (String s : classNameList) {
                    System.out.println("==============" + s + "===========");
                    execute(s);
                    System.out.println("=================结束==============");
                }
            }
        }
    }

    /**
     * execute
     */
    private void execute(String clz) {
        String pkg = ObjectToSQL.class.getPackage().getName();
        if ("".equals(clz) || clz == null) {
            clz = pkg + "." + TARGET.substring(1) + "." + CLASSNAME;
        }
        Class<?> cls;
        try {
            cls = Class.forName(clz);
            Field[] fields = cls.newInstance().getClass().getDeclaredFields();
            // update
            updateExecute(fields);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            System.out.println("=== 【类未找到】 ===");
        }
    }

    /**
     * update
     */
    private void updateExecute(Field[] fields) {
        System.out.println("update " + DBNAME + "\nset\nflowno = :flowno");
        ArrayUtil.reverse(fields);
        for (Field field : fields) {
            String f = field.getName().toLowerCase(Locale.ROOT);
            if ("SerialVersionUID".equalsIgnoreCase(f)) continue;
            System.out.println("#if(''!=" + f + " && null!=" + f + ")");
            System.out.println("," + f + " = :" + f);
            System.out.println("#end");
        }
        System.out.println("where\nflowno = :flowno");
    }
}