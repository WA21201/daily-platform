package com.wang.transfer.util.propertiesToJson.mapJson;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class FormatJson {

    // 一级map
    // LinkedHashMap entry双向链表结构，保证了插入的entry中的顺序
    private LinkedHashMap<String, List<HashMap<String, String>>> oneMap;

    public FormatJson() {
        this.oneMap = new LinkedHashMap<>();
    }

    public LinkedHashMap<String, List<HashMap<String, String>>> getOneMap() {
        return oneMap;
    }

    public void setOneMap(LinkedHashMap<String, List<HashMap<String, String>>> oneMap) {
        this.oneMap = oneMap;
    }
}
