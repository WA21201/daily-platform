package com.wang.transfer.util.propertiesToJson.mapJson;

import java.util.HashMap;

public class FormatJson2 {

    private HashMap<String, String> twoMap;

    public FormatJson2() {
        this.twoMap = new HashMap<>();
    }

    public HashMap<String, String> getTwoMap() {
        return twoMap;
    }

    public void setTwoMap(HashMap<String, String> twoMap) {
        this.twoMap = twoMap;
    }
}
