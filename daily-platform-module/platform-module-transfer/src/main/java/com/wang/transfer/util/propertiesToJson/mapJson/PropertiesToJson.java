package com.wang.transfer.util.propertiesToJson.mapJson;

import com.wang.common.utils.Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * properties内容转换成json格式的内容
 * key 与 value 之间采用的是 = 连接，可以在代码中修改
 *
 * @since : 2022.7.15
 */
public class PropertiesToJson {

    // key value 连接符号
    private static final String SYMBOL = "=";
    // 替换的字符集合，注意：这里一定要使用LinkedHashMap，确保替换字符的顺序问题
//    private static final LinkedHashMap<String, String> TRANSFER_SYMBOL = new LinkedHashMap<>();
    // 输入文件
    private static final String INPUT_FILE = "D:\\ProjectsInfo\\IdeaProjects\\daily-platform\\daily-platform-module\\platform-module-transfer\\src/main/resources/prop/messages_zh_CN.properties";
    // 输出文件
    private static final String OUTPUT_FILE = "D:\\ProjectsInfo\\IdeaProjects\\daily-platform\\daily-platform-module\\platform-module-transfer\\src/main/resources/json/cn_message.json";

    public static void main(String[] args) {

        // 实例化储存数据类型
        FormatJson formatJson = new FormatJson();

        // 读文件
        readFile(formatJson);

        // 写文件
        writeFile(formatJson);
    }

    /**
     * 读文件方法
     *
     * @param formatJson 读入的数据
     */
    public static void readFile(FormatJson formatJson) {
        BufferedReader reader = null;
        // 存储所有onePrefix的数量
        HashMap<String, Integer> list = new HashMap<>();
        try {
            reader = new BufferedReader(new FileReader(INPUT_FILE));
            String line;
            while ((line = reader.readLine()) != null) {
                line = Utils.checkLine(line, SYMBOL);
                if (line == null) {
                    continue;
                }
                // 行中不存在. || 行中存在.但.的索引大于=的索引
                if (!line.contains(".") || line.contains(".") && line.indexOf(".") > line.indexOf(SYMBOL)) {
                    // 例：poolServicePoolMember=票据池业务-池成员查询
                    String prefix = line.substring(0, line.indexOf(SYMBOL));
                    String suffix = line.substring(line.indexOf(SYMBOL) + 1);
                    // unicode transfer to chinese
                    suffix = Utils.unicodeToCN(suffix);
                    // \替换
                    suffix = Utils.replaceSlash(suffix);
                    if (prefix.equals("")) {
                        // key为空，不处理
                        continue;
                    }
                    ArrayList<HashMap<String, String>> hashMaps = new ArrayList<>();
                    HashMap<String, String> stringStringHashMap = new HashMap<>();
                    stringStringHashMap.put(prefix, suffix);
                    hashMaps.add(stringStringHashMap);
                    formatJson.getOneMap().put(prefix, hashMaps);
                    continue;
                }
                // 正常情况
                String prefix = line.substring(0, line.indexOf("."));
                String suffix = line.substring(line.indexOf(".") + 1);
                String twoPrefix = suffix.substring(0, suffix.indexOf(SYMBOL));
                String twoSuffix = suffix.substring(suffix.indexOf(SYMBOL) + 1);
                // unicode transfer to chinese
                twoSuffix = Utils.unicodeToCN(twoSuffix);
                // \替换
                twoSuffix = Utils.replaceSlash(twoSuffix);
                list.put(prefix, list.get(prefix) == null ? 1 : list.get(prefix) + 1);
                HashMap<String, String> twoMap = new FormatJson2().getTwoMap();
                twoMap.put(twoPrefix, twoSuffix);
                if (formatJson.getOneMap().containsKey(prefix)) {
                    // list里是否有同样的hashMap的key
                    List<HashMap<String, String>> hashMaps = formatJson.getOneMap().get(prefix);
                    boolean tag = true;
                    for (HashMap<String, String> map : hashMaps) {
                        if (map.containsKey(twoPrefix)) {
                            map.put(twoPrefix, twoSuffix);
                            tag = false;
                        }
                    }
                    if (tag) {
                        // formatJson中如果存在同样的key
                        formatJson.getOneMap().get(prefix).add(twoMap);
                    }
                    continue;
                }
                ArrayList<HashMap<String, String>> objects = new ArrayList<>();
                objects.add(twoMap);
                formatJson.getOneMap().put(prefix, objects);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 写文件方法
     *
     * @param formatJson 写入的数据
     */
    public static void writeFile(FormatJson formatJson) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(OUTPUT_FILE));
            // 加入{ 和 换行
            writer.write("{\n");
            Iterator<String> iterator = formatJson.getOneMap().keySet().iterator();
            while (iterator.hasNext()) {
                String next = iterator.next();
                List<HashMap<String, String>> hashMaps = formatJson.getOneMap().get(next);
                writer.append("\t").append('"').append(next).append('"').append(": ");
                if (hashMaps.size() == 1) {
                    HashMap<String, String> stringStringHashMap = hashMaps.get(0);
                    Iterator<String> iterator1 = stringStringHashMap.keySet().iterator();
                    String next1 = iterator1.next();
                    if (next1.equals(next)) {
                        writer.append('"').append(stringStringHashMap.get(next1)).append('"');
                        if (iterator.hasNext()) {
                            writer.append(",\n");
                        }
                        continue;
                    }
                }
                writer.append("{\n");
                for (int i = 0; i < hashMaps.size(); i++) {
                    HashMap<String, String> next1 = hashMaps.get(i);
                    Iterator<String> iterator1 = next1.keySet().iterator();
                    if (iterator1.hasNext()) {
                        String next2 = iterator1.next();
                        writer.append("\t\t").append('"').append(next2).append('"').append(": ").append('"').append(next1.get(next2)).append('"');
                    }
                    if (i < hashMaps.size() - 1) {
                        writer.append(",\n");
                    } else {
                        writer.append("\n\t}");
                    }
                }
                if (iterator.hasNext()) {
                    writer.append(",\n");
                }
            }
            writer.write("\n}");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
