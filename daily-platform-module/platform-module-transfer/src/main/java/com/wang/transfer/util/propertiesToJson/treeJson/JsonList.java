package com.wang.transfer.util.propertiesToJson.treeJson;

import java.util.ArrayList;

public class JsonList {

    private ArrayList<JsonTree> jsonTrees;

    public JsonList() {
        this.jsonTrees = new ArrayList<>();
    }

    public ArrayList<JsonTree> getJsonTrees() {
        return jsonTrees;
    }

    public void setJsonTrees(ArrayList<JsonTree> jsonTrees) {
        this.jsonTrees = jsonTrees;
    }
}
