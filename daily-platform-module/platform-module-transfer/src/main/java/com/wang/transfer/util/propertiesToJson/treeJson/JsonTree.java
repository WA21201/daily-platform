package com.wang.transfer.util.propertiesToJson.treeJson;

import java.util.ArrayList;

public class JsonTree {

    private String name;
    private ArrayList<JsonTree> children;

    public JsonTree() {
        this.name = null;
        this.children = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<JsonTree> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<JsonTree> children) {
        this.children = children;
    }
}
