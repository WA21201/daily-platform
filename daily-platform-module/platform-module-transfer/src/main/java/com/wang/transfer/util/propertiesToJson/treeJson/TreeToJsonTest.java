package com.wang.transfer.util.propertiesToJson.treeJson;

import com.wang.transfer.util.propertiesToJson.treeJson.exception.InputOutputFileException;

/**
 * 测试：Tree to Json 类
 */
public class TreeToJsonTest {

    public static void main(String[] args) {

        TreeToJson treeToJson;
        String inputFile = "D:\\ProjectsInfo\\IdeaProjects\\daily-platform\\daily-platform-module\\platform-module-transfer\\src\\main\\resources\\prop\\messages_zh_CN.properties";
        String outputFile = "D:\\ProjectsInfo\\IdeaProjects\\daily-platform\\daily-platform-module\\platform-module-transfer\\src/main/resources/json/message_zh_CN.json";
        try {
            treeToJson = new TreeToJson(inputFile, outputFile);
            long first = System.currentTimeMillis();
            treeToJson.execute();
            long last = System.currentTimeMillis();
            System.out.println(last - first);
        } catch (InputOutputFileException e) {
            e.printStackTrace();
        }

    }
}
