package com.wang.transfer.util.propertiesToJson.treeJson.exception;

/**
 * 输入文件路径为空异常
 */
public class InputFileEmptyException extends InputOutputFileException {

    public InputFileEmptyException() {
        super();
    }

    public InputFileEmptyException(String message) {
        super(message);
    }
}
