package com.wang.transfer.util.propertiesToJson.treeJson.exception;

import java.io.IOException;

/**
 * 输入输出数据到文件异常
 */
public class InputOutputFileException extends IOException {

    public InputOutputFileException() {
        super();
    }

    public InputOutputFileException(String message) {
        super(message);
    }
}
