package com.wang.transfer.util.propertiesToJson.treeJson.exception;

/**
 * 输出文件路径为空异常
 */
public class OutputFileEmptyException extends InputOutputFileException{

    public OutputFileEmptyException() {
        super();
    }

    public OutputFileEmptyException(String message) {
        super(message);
    }
}
