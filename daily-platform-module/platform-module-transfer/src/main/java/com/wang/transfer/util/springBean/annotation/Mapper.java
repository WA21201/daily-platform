package com.wang.transfer.util.springBean.annotation;

import java.lang.annotation.*;

/**
 * Dao层注解 类似 Mapper
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
public @interface Mapper {
}
