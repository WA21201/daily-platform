package com.wang.transfer.util.springBean.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 依赖注入 类似 @Resource <br>
 * 有名称则按照名称注入，否则按照类型注入
 */
@Target({TYPE, FIELD, METHOD})
@Retention(RUNTIME)
public @interface Res {

    /**
     * 注入名称
     */
    String name() default "";

    /**
     * 注入类型
     */
    Class<?> type() default Object.class;
}
