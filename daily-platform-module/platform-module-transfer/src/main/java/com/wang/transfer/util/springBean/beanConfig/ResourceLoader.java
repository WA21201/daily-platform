package com.wang.transfer.util.springBean.beanConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 资源加载器，就是来完成这个工作的，由它来完成配置文件中配置的加载。
 */
public class ResourceLoader {

    public static Map<String, BeanDefinition> getResource() {

        Map<String, BeanDefinition> beanDefinitionMap = new HashMap<>(16);

        Properties properties = new Properties();

        InputStream inputStream = null;

        try {
            inputStream = ResourceLoader.class.getResourceAsStream("/beans.properties");
            properties.load(inputStream);
            for (String key : properties.stringPropertyNames()) {
                String className = properties.getProperty(key);
                BeanDefinition beanDefinition = new BeanDefinition();
                beanDefinition.setBeanName(key);
                Class<?> clazz = Class.forName(className);
                beanDefinition.setBeanClass(clazz);
                beanDefinitionMap.put(key, beanDefinition);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return beanDefinitionMap;
    }
}
