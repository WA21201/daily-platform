package com.wang.transfer.util.springBean.dao;

import com.wang.transfer.util.springBean.annotation.Mapper;

@Mapper
public class UserDaoImpl implements UserDao{

    @Override
    public void queryUser() {
        System.out.println("queryUser...");
    }
}
