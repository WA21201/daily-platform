package com.wang.transfer.util.springBean.exception;

/**
 * 可解释的通用错误码
 */
public interface ExplicableErrorCode {

    /**
     * 获取描述信息
     */
    public String getMsg();

    /**
     * 返回错误码
     */
    public String getCode();
}
