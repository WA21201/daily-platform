package com.wang.transfer.util.springBean.exception;

public enum LoanManageErrorCode implements ExplicableErrorCode{

    /**
     * 还款本金金额大于剩余本金金额
     */
    REPAY_PRINCIPAL_IS_GREATER_THAN_PRINCIPAL("repay principal(%s) is greater than rest principal (%s)"),

    /**
     * 剩余本金为负
     */
    REST_PRINCIPAL_IS_NEGATIVE("rest principal (%s) is negative");

    private final String msg;

    LoanManageErrorCode(String msg) {
        this.msg = msg;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public String getCode() {
        return this.name();
    }
}
