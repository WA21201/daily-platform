package com.wang.transfer.util.springBean.exception;

import java.util.function.Function;

/**
 * 对方法调用进行封装，捕获异常
 */
public class ResponseProcessor {

    public static <T, R extends String> R handle(T request, R response, String method, Function<T, R> function) {

        try {
            response = function.apply(request);

            return response;
        } catch (Exception e) {
            // response.set....
        }
        return response;
    }

}
