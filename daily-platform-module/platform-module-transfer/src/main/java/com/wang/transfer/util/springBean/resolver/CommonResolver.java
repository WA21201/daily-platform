package com.wang.transfer.util.springBean.resolver;

import com.wang.transfer.util.springBean.beanConfig.BeanFactory;

import java.util.ArrayList;
import java.util.logging.Logger;

public class CommonResolver {

    public final Logger logger = Logger.getLogger(CommonResolver.class.getName());

    public CommonResolver(BeanFactory beanFactory) {

        // 获取启动类下所在包下所有的类
        ArrayList<Class> allClass = beanFactory.getAllClass(beanFactory.getClass().getPackage().getName());
        logger.info("Mapper 所有类：" + allClass);

        // 调用Mapper注解解析器
        new MapperAnnotationResolver(beanFactory, allClass);

        // 调用Res注解解析器
        new ResAnnotationResolver(beanFactory, allClass);
    }
}
