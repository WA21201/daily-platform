package com.wang.transfer.util.springBean.util;

/**
 * 工具类
 */
public class SpringUtils {

    /**
     * 格式化bean名称
     * @param clazz bean类
     * @return beanName
     */
    public static String beanNameFormat(Class<?> clazz) {
        String beanName = clazz.getName().substring(clazz.getName().lastIndexOf(".") + 1);
        beanName = beanName.substring(0, 1).toLowerCase() + beanName.substring(1);
        return beanName;
    }
}
