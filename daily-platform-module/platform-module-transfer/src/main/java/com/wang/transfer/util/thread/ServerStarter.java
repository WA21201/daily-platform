package com.wang.transfer.util.thread;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ServerStarter extends Thread{

    @Async("taskExecutor")
    public void startServer() {
        System.out.println(Thread.currentThread().getName() + "执行业务逻辑...");
    }

    @Override
    public void run() {
        this.startServer();
    }
}
