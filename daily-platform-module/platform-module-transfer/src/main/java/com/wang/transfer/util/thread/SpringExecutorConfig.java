package com.wang.transfer.util.thread;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 使用spring提供的线程池包
 */
@EnableAsync
@Configuration
public class SpringExecutorConfig {

    @Bean("taskExecutor")
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        int i = Runtime.getRuntime().availableProcessors();//获取服务器的cpu内核
        executor.setCorePoolSize(10); //核心线程数
//        executor.setMaxPoolSize(100); //最大线程数
//        executor.setQueueCapacity(1000); //队列程度
//        executor.setThreadNamePrefix("task-async"); //线程前缀名称
//        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy()); //配置拒绝策略
        return executor;
    }
}
