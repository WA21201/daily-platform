package com.wang.transfer.util.validTest;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * 字段校验测试类
 * @since 2022-12-12
 */
@Data
public class VUser {

    @NotBlank(message = "id不能为空", groups = {ValidGroup.Query.class, ValidGroup.Delete.class, ValidGroup.Update.class})
    @Min(value = 3, message = "id不能小于3")
    @Length(min = 3, message = "id长度不能小于3", groups = {ValidGroup.Query.class, ValidGroup.Delete.class, ValidGroup.Update.class})
    private String id;

    @NotBlank(message = "用户名不能为空", groups = {ValidGroup.Insert.class, ValidGroup.Update.class})
    private String name;

    @NotBlank(message = "邮箱不能为空", groups = {ValidGroup.Insert.class, ValidGroup.Update.class})
    @Email(message = "邮箱格式不正确", groups = {ValidGroup.Insert.class, ValidGroup.Update.class})
    private String email;
}
