package com.wang.transfer.util.validTest;

import javax.validation.GroupSequence;

/**
 * 校验分组策略-定义分组
 * @since 2022-12-12
 */
public class ValidGroup {

    interface Query{}

    interface Insert{}

    interface Update{}

    interface Delete{}

    @GroupSequence({Query.class, Insert.class, Update.class, Delete.class})
    interface All{}
}
