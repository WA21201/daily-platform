package com.wang.transfer.util.validTest;

import com.wang.common.annotation.Log;
import com.wang.common.common.AResponse;
import com.wang.common.utils.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 校验测试
 */
@RestController
@Slf4j
@RequestMapping("/validTest")
public class ValidTestController {

    @PostMapping("/query")
    @Log(clazz = "ValidTestController", method = "query")
    public AResponse<VUser> query(@RequestBody @Validated(ValidGroup.Query.class) VUser user) {
        return ResultUtils.success(user);
    }

    @PostMapping("/insert")
    @Log(clazz = "ValidTestController", method = "insert")
    public AResponse<VUser> insert(@RequestBody @Validated(ValidGroup.Insert.class) VUser user) {
        return ResultUtils.success(user);
    }

    @PostMapping("/update")
    @Log(clazz = "ValidTestController", method = "update")
    public AResponse<VUser> update(@RequestBody @Validated(ValidGroup.Update.class) VUser user) {
        return ResultUtils.success(user);
    }

    @PostMapping("/delete")
    @Log(clazz = "ValidTestController", method = "delete")
    public AResponse<VUser> delete(@RequestBody @Validated(ValidGroup.Delete.class) VUser user) {
        return ResultUtils.success(user);
    }

    @PostMapping("/all")
    @Log(clazz = "ValidTestController", method = "all")
    public AResponse<VUser> all(@RequestBody @Validated(ValidGroup.All.class) VUser user) {
        return ResultUtils.success(user);
    }
}
