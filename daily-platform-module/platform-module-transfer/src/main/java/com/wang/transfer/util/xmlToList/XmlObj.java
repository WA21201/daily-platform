package com.wang.transfer.util.xmlToList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * xml to ObjList
 */
public class XmlObj {

    private String tag;
    private String content;
    private Boolean exist;
    private List<XmlObj> xmlObjs;

    public XmlObj() {
        this.tag = null;
        this.content = null;
        this.exist = false;
        this.xmlObjs = new ArrayList<>();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getExist() {
        return exist;
    }

    public void setExist(Boolean exist) {
        this.exist = exist;
    }

    public List<XmlObj> getXmlObjs() {
        return xmlObjs;
    }

    public void setXmlObjs(List<XmlObj> xmlObjs) {
        this.xmlObjs = xmlObjs;
    }

    @Override
    public String toString() {
        return resXmlObj(this);
    }

    /**
     * 循环子对象
     */
    private String resXmlObj(XmlObj obj) {
        StringBuilder sb = new StringBuilder();
        sb.append("{tag:").append(obj.getTag()).append(",content:").append(obj.getContent()).append(",child:");
        if (obj.getXmlObjs().size() > 0) {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < obj.getXmlObjs().size(); i++) {
                s.append(resXmlObj(obj.getXmlObjs().get(i))).append("},");
                if (i == obj.getXmlObjs().size() - 1) {
                    return sb.append(sb).toString();
                }
            }
        }
        return sb.toString();
    }
}
