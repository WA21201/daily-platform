package com.wang.transfer.util.xmlToList;

import com.wang.transfer.util.propertiesToJson.treeJson.exception.InputOutputFileException;

public class XmlTest {

    public static void main(String[] args) {

        XmlToObj xmlToObj = new XmlToObj();
        try {
            xmlToObj.execute();
        } catch (InputOutputFileException e) {
            e.printStackTrace();
        }
    }
}
