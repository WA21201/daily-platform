import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import pinia from './store'
import mitt from 'mitt'
import Antd, {message, notification} from 'ant-design-vue'
import * as Icons from "@ant-design/icons-vue"
import 'ant-design-vue/dist/antd.css'
import zhCN from 'ant-design-vue/es/locale/zh_CN';
import dayjs from 'dayjs'
import 'dayjs/locale/zh-cn';

const app = createApp(App)
app.use(pinia)
    .use(Antd)
    .use(router)
    .mount('#app')

/**
 * 全局事件总线
 */
app.config.globalProperties.$mitt = new mitt()

/**
 * dayjs
 */
dayjs.locale('zh')
app.config.globalProperties.zhCN = zhCN

/**
 * icons-vue全局引入
 */
const icons = Icons
for (let i in icons) {
    app.component(i, icons[i])
}

/**
 * 通知提示框
 * @param type 类型
 * @param message 消息
 * @param desc 详情
 */
const Notice = [];
const common = (type, message, desc) => {
    notification[type]({
        message: message,
        description: desc,
    })
}
const error = (message, desc) => {
    common('error', message, desc)
}
const success = (message, desc) => {
    common('success', message, desc)
}
const info = (message, desc) => {
    common('info', message, desc)
}
const warning = (message, desc) => {
    common('warning', message, desc)
}
Notice.error = error;
Notice.success = success;
Notice.info = info;
Notice.warning = warning;
app.config.globalProperties.$Notice = Notice;

/**
 * message全局提示
 */
app.config.globalProperties.$Message = message
