import {createRouter, createWebHistory} from 'vue-router'
import Home from "@/views/Home"
import Login from "@/views/Login";
import Index from "@/views/Index"
import {notification} from "ant-design-vue";
import Content from "@/views/Content";
import AllMenu from "@/views/menu/AllMenu";
import UserMenu from "@/views/menu/UserMenu";

const notice = (message, desc) => {
    notification['error']({
        message: message,
        description: desc,
    })
}

const routes = [
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/',
        name: 'home',
        component: Home,
        children: [
            {
                path: '',
                name: 'index',
                component: Index
            },
            {
                path: 'content',
                name: 'content',
                component: Content,
                children: [
                    {
                        path: 'allMenu',
                        name: '0006',
                        component: AllMenu
                    },
                    {
                        path: 'userMenu',
                        name: '0007',
                        component: UserMenu
                    }
                ]
            }
        ]
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about"  '../views/AboutView.vue'*/)
    // }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach((to, from) => {
    let token = sessionStorage.token
    if (!token && to.name !== 'login') {
        localStorage.removeItem('userData')
        notice("登录超时","登录超时，请重新登录！")
        return 'login'
    } else if (token && to.name === 'login') {
        return '/'
    }
    return true
})

export default router
