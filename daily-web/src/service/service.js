import fetch from '../utils/fetch';
import {notification} from "ant-design-vue";

const notice = (message, desc) => {
    notification['error']({
        message: message,
        description: desc,
    })
}

/**
 * 接口api请求，调用方式请参考登录页面登录接口
 */
export const apiRequest = (apiFunc, success, error) => apiFunc.then(response => {
    if (response.code === 441) {
        sessionStorage.removeItem('token')
        localStorage.removeItem('userData')
        notice(response.message, response.description);
        location.reload()
    } else if (response.code !== 200) {
        notice(response.message, response.description);
    } else {
        // 返回正常数据给接口调用者
        success(response);
    }
    if (error) error()
}).catch(function (error) { // 加上catch
    notice('错误消息提示', error);
    console.log(error);
});

/**
 * pro to json
 */
export const propertiesToJson = () => fetch('/treeJson/propertiesToJson', {}, 'POST');

/**
 * ========================================================用户操作======================================================
 */
// 登录
export const login = (userInfo) => fetch('/user/login', userInfo, 'POST');
// 注册
export const register = (userInfo) => fetch('/user/register', userInfo, 'POST');
// 登出
export const logout = () => fetch('/user/logout', {}, 'GET');
// 头像上传
export const uploadHeadImage = () => fetch('/user/uplaodHeadImage', {}, '');
// 修改用户信息
export const updateUserInfo = (userInfo) => fetch('/user/updateUserInfo', userInfo, 'POST');
// 忘记密码
export const forgetPassword = (userInfo) => fetch('/user/forgetPassword', userInfo, "POST");

/**
 * ========================================================问题操作======================================================
 */
// 查询问题列表
export const queryProblem = () => fetch('/problem/queryProblem', {}, 'GET');

/**
 * ========================================================菜单操作======================================================
 */
// 查询用户菜单
export const queryUserMenu = () => fetch('/userMenu/queryUserMenu', {}, 'GET');
// 分页条件查询所有菜单
export const queryAllMenu = (searchInfo) => fetch('/menu/queryMenu', searchInfo, 'POST');
// 保存/新增菜单
export const saveMenu = (saveInfo) => fetch('/menu/saveMenu', saveInfo, 'POST');
// 删除菜单
export const deleteMenu = (deleteInfo) => fetch('/menu/deleteMenu', deleteInfo, 'POST');

/**
 * =====================================================用户菜单操作======================================================
 */
// 查询所有用户菜单分页
export const queryAllUserMenu = (searchInfo) => fetch('/userMenu/queryAllUserMenu', searchInfo, 'POST');
// 新增/保存用户菜单
export const saveAllUserMenu = (saveInfo) => fetch('/userMenu/saveUserMenu', saveInfo, 'POST');
