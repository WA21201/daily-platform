import { createPinia } from 'pinia'
import menuStore from "@/store/menu";

const pinia = createPinia()

export default pinia
