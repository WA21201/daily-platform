import {defineStore} from "pinia";

const useMenu = defineStore('menu', {
    state: () => {
        return {
            current: []
        }
    },
    actions: {
        curPush(key) {
            this.current.pop()
            this.current.push(key)
        }
    }
})
export default useMenu
