let baseUrl = '/api';
require("es6-promise").polyfill();
require('isomorphic-fetch');

export default async(url = '', data = {}, type = 'POST', method = 'fetch') => {
	type = type.toUpperCase();
	url = baseUrl + url;

	if (type === 'GET') {
		let dataStr = ''; //数据拼接字符串
		Object.keys(data).forEach(key => {
			dataStr += key + '=' + data[key] + '&';
		})

		if (dataStr !== '') {
			dataStr = dataStr.substr(0, dataStr.lastIndexOf('&'));
			url = url + '?' + dataStr;
		}
	}

	if (window.fetch && method === 'fetch') {
		let token = sessionStorage.token;
		token = !token||token==='undefined' ? null : token;
		let requestConfig = {
			credentials: 'include',
			method: type,
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'token': token,
				'daily-proxy':'daily-proxy',
				'Cache-Control':'no-cache',
				'Pragma':'no-cache'
			},
			mode: "cors",
			cache: "force-cache"
		}

		if (type === 'POST' || type === 'PUT') {
			Object.defineProperty(requestConfig, 'body', {
				value: JSON.stringify(data)
			})
		}else if (type === 'FILE'){
			requestConfig = {
				method: 'POST',body: data,
				'Accept': 'application/json'
			}
		}

		try {
			const response = await fetch(url, requestConfig);
			switch (response.status) {
				case 200:break;
				case 410:
					    return {code:response.status, message:'请求参数错误'}
				case 420:
						return {code:response.status, message:'请求参数为空'}
				case 430:
						return {code:response.status, message:'无权限'}
				case 440:
						return {code:response.status, message:'未登录'}
				case 441:
						return {code:response.status, message:'登录超时'}
				case 401:
						return {code:response.status, message:'用户无菜单权限'}
				case 510:
						return {code:response.status, message:'系统内部错误'}
				default:
				        return {code:response.status, message:'无法连接服务器，请检查网络是否可用'}
			}
			return await response.json();
		} catch (error) {
			return error;
		}
	} else {
		return new Promise((resolve, reject) => {
			let requestObj;
			if (window.XMLHttpRequest) {
				requestObj = new XMLHttpRequest();
			} else {
				requestObj = new ActiveXObject;
			}

			let sendData = '';
			if (type === 'POST'|| type === 'PUT') {
				sendData = JSON.stringify(data);
			}

			requestObj.open(type, url, true);
			requestObj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			requestObj.send(sendData);

			requestObj.onreadystatechange = () => {
				if (requestObj.readyState === 4) {
					if (requestObj.status === 200) {
						let obj = requestObj.response
						if (typeof obj !== 'object') {
							obj = JSON.parse(obj);
						}
						resolve(obj)
					} else {
						reject(requestObj)
					}
				}
			}
		})
	}
}
