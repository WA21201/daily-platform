import fingerCheckPage from '@/components/finger.vue'

const tools = {
    name: 'tools dayo~',
    jsonDateFormat: (jsonDate) => {
        // json日期格式转换为正常格式
        try {
            const date = new Date(jsonDate);
            const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
            const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            return date.getFullYear() + '-' + month + '-' + day;
        } catch (ex) {
            return '';
        }
    },
    getDateFormat: function (date) {
        return date.toJSON().substring(0, 10);
    },
    getMonthFormat: function (date) {
        return date.toJSON().substring(0, 7);
    },
    getToday:function(){
        return new Date();
    },
    getYesterday: function (){
      let date = new Date();
      date.setDate(date.getDate() -1);
      return date;
    },
    getTomorrow: function () {
        /**
         * 本日+1
         * @type {Date}
         */
        let date = new Date();
        date.setDate(date.getDate()+1);
        return date;
    },
    getPlusDay: function (plus) {
        let date = this.getToday();
        date.setDate(date.getDate() + plus);
        return date;
    },
    getLastMonthDay: function () {
        let date = new Date();
        var year;
        var month = date.getMonth() - 1;
        if(month === 0){
            year = date.getFullYear() - 1;
            month = 12;
            date.setFullYear(year);
        }
        date.setMonth(month);
        return date;
    },
    getLastThreeMonthDay: function () {
        let date = new Date();
        var year;
        var month = date.getMonth() - 3;
        if(month < 1){
            year = date.getFullYear() - 1;
            month = month + 12;
            date.setFullYear(year);
        }
        date.setMonth(month);
        return date;
    },
    getFirstDayOfYear: function () {
        let date = new Date();
        date.setMonth(0);
        date.setDate(1);
        return date;
    },
    getTodayOfNextYear: function () {
        let date = this.getToday();
        date.setFullYear(date.getFullYear() + 1);
        return date;
    },
    format: function (fmt, date) {
        var o = {
            'M+': date.getMonth() + 1, // 月份
            'd+': date.getDate(), // 日
            'h+': date.getHours(), // 小时
            'm+': date.getMinutes(), // 分
            's+': date.getSeconds(), // 秒
            'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
            'S': date.getMilliseconds() // 毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
        for (var k in o) { if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length))); }
        return fmt;
    },
    compareDate: function (d1, d2) {
        return ((new Date(d1.replace(/-/g, '\/'))) > (new Date(d2.replace(/-/g, '\/'))));
    },
    // start:开始日期，end:结束日期，days:日期范围
    diffDate: (start, end, days) => {
        var msg = '验证成功';
        let starttime = start.replace(/-/g, '/');
        let endtime = end.replace(/-/g, '/');
        let starttimes = new Date(starttime.substring(0, 4), starttime.substring(4, 6), starttime.substring(6, 8));
        let endtimes = new Date(endtime.substring(0, 4), endtime.substring(4, 6), endtime.substring(6, 8));
        if (starttimes > endtimes) {
            msg = '请选择正确的日期区间';
            return msg;
        }
        if (endtimes - starttimes > 86400000 * days) {
            msg = '可选日期范围为' + days + '天';
            return msg;
        }
        return msg;
    },
    isRepeat: function (arr) {
        var hash = {};
        for (var i in arr) {
            if (hash[arr[i]]) { return true; }
            hash[arr[i]] = true;
        }
        return false;
    },
    adaptTableWidth: function (a, b, c) {
        //取消表格东台宽度



        // /**
        //  * a:当前页面上下文
        //  * b:表格所处页面
        //  * c:多个表格时传入存放表格列信息的数组名
        //  */
        // if (b === null || b === undefined) {
        //     b = 'single-page';
        // }
        // if (c === null || c=== undefined) {
        //     c = 'columns';
        // }
        // // 根据表格所在区域总宽度设置各列宽度
        // var elm = document.getElementsByClassName(b);
        // // 区域总宽度
        // var wrapWidth = elm[0].clientWidth;
        // // 表格列信息在页面上都是作为数组储存的
        // var array = a.$data[c];
        // var count = array.length;
        // for (let item of array) {
        //     if (item.width !== undefined) {
        //         // 从总宽度中去除有确定宽度的列的宽度
        //         wrapWidth = wrapWidth - item.width;
        //         // 从总列数中减去有确定宽度列
        //         count = count - 1;
        //     }
        // }
        // // 总宽度减去确定宽度列宽 / 非确定宽度列数 = 非确定宽度列宽
        // var collength = Math.round(wrapWidth / count);
        // // 非确定宽度列宽不小于100
        // if (collength < 150) {
        //     collength = 150;
        // }
        // for (let item of array) {
        //     // 为非确定宽度列赋宽度
        //     if (item.width === undefined) {
        //         a.$set(item, 'width', collength);
        //     }
        // }
    },
    fingerCheck: function(a, data) {
        a.$Modal.confirm({
            title: "指纹校验",
            width:'80%',
            render: (h) => {
                return h(fingerCheckPage, {
                    props:{data:data}
                })
            }
        });
    },
    //乘法函数，用来得到精确的乘法结果
    //原理：将小数化为整数再进行运算  10^m /10^m = 1
    accMul:function(arg1,arg2){
        if(!!arg1 && !!arg2){
            var m=0;
            //将两个相乘的数arg1,arg2化为字符串。
            var s1=new Number(arg1).toString();
            var s2=new Number(arg2).toString();
            //分别获取两个数的小数点后位数并相加和为m
            try{m+=s1.split(".")[1].length}catch(e){}
            try{m+=s2.split(".")[1].length}catch(e){}
            //去除两个数的小数点(即化为整数) 在最后除去10的m次方
            return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m)
        }
    },
    //除法函数，用来得到精确的除法结果
    //原理：将小数化为整数再进行运算   10^t1/10^t2 * 10^(t2-t1) =1
    accDiv:function(arg1,arg2){
        if(!!arg1 && !!arg2) {
            var t1 = 0, t2 = 0;
            //将两个相乘的数arg1,arg2化为字符串。
            var s1=new Number(arg1).toString();
            var s2=new Number(arg2).toString();
            //分别获取两个数的小数点后位数 t1 t2
            if(s1.split(".").length >1){
                t1 = s1.split(".")[1].length;
            }
            if(s2.split(".").length >1){
                t2 = s2.split(".")[1].length;
            }
            return (Number(s1.replace(".", "")) / Number(s2.replace(".", "")) * Math.pow(10, t2 - t1))
        }
    },
    functionRequest:function(url,filename){
        let requestObj;
        if (window.XMLHttpRequest) {
            requestObj = new XMLHttpRequest();
        } else {
            requestObj = new ActiveXObject;
        }
        requestObj.open("GET", url, true);
        requestObj.setRequestHeader("Content-Disposition","attachment;filename="+filename);
        requestObj.setRequestHeader("Content-type","application/octet-stream");
    },
    functionGet:function(url,filename){
        var form = document.createElement("form");
        form.action = url;
        form.method = "GET";
        document.body.appendChild(form);
        form.submit();
        document.body.removeChild(form);
    }
}
export default tools;
