const utils = {
    /**
     * 限制用户名 只能包含中文、英文和数字，不能包含特殊字符
     * /^[\u4E00-\u9FA5A-Za-z0-9]+$/
     */
    hasSpecialChar(str) {
        return !/^[\u4E00-\u9FA5A-Za-z0-9]+$/.test(str.trim())
    },
}
export default utils;
