export const valids = {

    /**
     * 公共最短最长长度判断
     */
    MinMaxLength(minLen, maxLen, label) {
        if (minLen === maxLen) {
            return label + "的长度应为" + minLen + "！"
        }
        return label + "的长度范围为" + minLen + "-" + maxLen + "之间！"
    },

    /**
     * 默认校验规则
     * 1、值是否存在
     * 2、值的长度限定
     * 3、字段名称
     */
    valid({minLen = 0, maxLen = 0, label}) {
        return async (_rule, value) => {
            if (!value) {
                return Promise.reject("请输入" + label + "！")
            } else if (value.length < minLen || value.length > maxLen) {
                return Promise.reject(this.MinMaxLength(minLen, maxLen, label))
            }
            return Promise.resolve()
        }
    },

    /**
     * 1、值存在
     * 2、正整数
     * 3、值的长度限定
     * 4、字段名称
     */
    validI({minLen = 0, maxLen = 0, label}) {
        return async (_rule, value) => {
            if (!value) {
                return Promise.reject("请输入" + label + "！")
            } else if (/\D/g.test(value)) {
                return Promise.reject("请输入正整数！")
            } else if (value.length < minLen || value.length > maxLen) {
                return Promise.reject(this.MinMaxLength(minLen, maxLen, label))
            }
            return Promise.resolve()
        }
    },

    /**
     * 1、值存在
     * 2、正整数
     * 3、值的长度固定
     * 4、字段名称
     */
    validIL({len, label}) {
        return this.validI({minLen: len, maxLen: len, label})
    },

    /**
     * 1、值存在
     * 2、值的长度固定
     * 3、字段名称
     */
    validL({len, label}) {
        return this.valid({minLen: len, maxLen: len, label})
    },

    /**
     * 1、值存在、长度限定
     * 2、字段名称
     */
    validVL({minLen = 0, maxLen = 0, label}) {
        return async (_rule, value) => {
            if (!value || value.length < minLen || value.length > maxLen) {
                return Promise.reject(this.MinMaxLength(minLen, maxLen, label))
            }
            return Promise.resolve()
        }
    },

    /**
     * 1、值存在
     */
    validV({msg}) {
        return async (_rule, value) => {
            if (!value) {
                return Promise.reject(msg)
            }
            return Promise.resolve()
        }
    }
}
