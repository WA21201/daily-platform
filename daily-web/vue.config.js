const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    lintOnSave: false,
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:8888',
                // 重写请求路径
                pathRewrite: {'^/api': ''},
                ws: false, // 用于支持websocket
                changeOrigin: true
            }
        }
    }
})
